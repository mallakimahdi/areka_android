package ir.areka.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ir.areka.app.R;
import ir.areka.app.model.comment.Comment;

public class CommentListAdapter extends ArrayAdapter<Comment> {
    private LayoutInflater inflater;
    public static Context mContext;

    public CommentListAdapter(Context context, List<Comment> comments) {
        super(context, R.layout.row_comment, comments);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Comment item = getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_comment, null);
            holder.txtComment = (TextView) convertView.findViewById(R.id.txt_comment_comment);
            holder.txtUsername = (TextView) convertView.findViewById(R.id.txt_comment_username);
            holder.viewStars = (LinearLayout) convertView.findViewById(R.id.ll_comment_stars);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.fill(item);

        return convertView;
    }

    private static class ViewHolder {
        public TextView txtComment;
        public TextView txtUsername;
        public LinearLayout viewStars;

        public void fill(Comment comment) {
            txtUsername.setText(comment.getUsername() + "");
            txtComment.setText(comment.getComment() + "");
            viewStars.removeAllViews();

            if(comment.getStars() != null) {
                for (int j = 0; j < comment.getStars(); j++) {
                    ImageView img = new ImageView(mContext);
                    img.setImageResource(R.drawable.ic_action_star);
                    viewStars.addView(img);
                }
            }
        }
    }
}
