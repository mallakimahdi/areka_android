package ir.areka.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.model.suggestion.Suggestion;

/**
 * Created by mahdi on 7/7/16.
 */
public class SuggestListAdapter extends ArrayAdapter<Suggestion> implements Filterable {

    private List<Suggestion> items;
    private CustomFilter customFilter;
    protected LayoutInflater inflater;

    public SuggestListAdapter(Context context) {
        super(context, android.R.layout.simple_dropdown_item_1line);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        items = new ArrayList<>();
    }

    @Override
    public void add(Suggestion object) {
        items.add(object);
        notifyDataSetChanged();
    }

    @Override
    public void addAll(Collection<? extends Suggestion> collection) {
        items.addAll(collection);
        notifyDataSetChanged();
    }

    @Override
    public void clear() {
        items.clear();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Suggestion getItem(int position) {

        if(position >= items.size()) {
            return null;
        }
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Suggestion item = getItem(position);

        if(item == null)
            return null;

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.row_suggestion, null);
            holder.txtName = (TextView) convertView.findViewById(R.id.txt_suggestion);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.fill(item);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (customFilter == null)
            customFilter = new CustomFilter();
        return customFilter;
    }

    public void callFiltering() {
        customFilter.performFiltering("");
    }

    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                results.values = items;
                results.count = items.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results != null && results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }

    private static class ViewHolder {

        public TextView txtName;

        public void fill(Suggestion item) {
            txtName.setText(item.toString());
        }
    }
}
