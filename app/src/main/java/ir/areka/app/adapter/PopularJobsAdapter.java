package ir.areka.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ir.areka.app.R;
import ir.areka.app.model.job.PopularJob;
import ir.areka.app.util.WordNormalizer;

/**
 * Created by mahdi on 9/18/16.
 */
public class PopularJobsAdapter extends ArrayAdapter<PopularJob> {

    private final LayoutInflater inflater;
    private static Context context;

    public PopularJobsAdapter(Context context, List<PopularJob> jobs) {
        super(context, R.layout.row_popular_jobs, jobs);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PopularJob item = getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_popular_jobs, null);
            holder.rootView = convertView;
            holder.txtJobKey = (TextView) convertView.findViewById(R.id.txt_job_key);
            holder.txtJobCount = (TextView) convertView.findViewById(R.id.txt_job_count);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.fill(item);

        return convertView;
    }

    private static class ViewHolder {

        public View rootView;
        public TextView txtJobCount;
        public TextView txtJobKey;

        public void fill(PopularJob item) {
            txtJobKey.setText(item.getKey());
            txtJobCount.setText(context.getString(R.string.space) + "("
                    + WordNormalizer.getInstance().toArabicDigits(item.getDoc_count()) +") ");
            ((ImageView) rootView.findViewById(R.id.img_show_jobs)).setColorFilter(context.getResources().getColor(R.color.more_details_flag));
        }
    }
}
