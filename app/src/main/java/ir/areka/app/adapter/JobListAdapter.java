package ir.areka.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ir.areka.app.R;
import ir.areka.app.activity.FavoriteJobsActivity;
import ir.areka.app.fragment.OnCompanySelected;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.util.DateUtil;

/**
 * Created by mahdi on 3/22/16.
 */
public class JobListAdapter extends ArrayAdapter<Job> {

    private final LayoutInflater inflater;
    private static Context context;

    public JobListAdapter(Context context, List<Job> jobs) {
        super(context, R.layout.row_search_result, jobs);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Job item = getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_search_result, null);
            holder.mainView = convertView;
            holder.txtJobTitle = (TextView) convertView.findViewById(R.id.txt_job_title);
            holder.txtCompanyName = (TextView) convertView.findViewById(R.id.txt_search_company_name);
            holder.txtMoreDetails = (TextView) convertView.findViewById(R.id.txt_job_more_details);
            holder.txtNewJob = (TextView) convertView.findViewById(R.id.txt_is_job_new);
            holder.imgFavorite = (ImageView) convertView.findViewById(R.id.img_favorite);
            holder.rootView = convertView.findViewById(R.id.ll_search_row_root);
            holder.txtLastVisitedDate = (TextView) convertView.findViewById(R.id.last_visited_date);
            holder.llCompanyName = (LinearLayout) convertView.findViewById(R.id.ll_search_company_name);
            holder.llHighlight = (LinearLayout) convertView.findViewById(R.id.ll_highlight);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.fill(item, position);

        return convertView;
    }

    private class ViewHolder {

        public View mainView;
        public View rootView;
        public TextView txtNewJob;
        public TextView txtJobTitle;
        public ImageView imgFavorite;
        public TextView txtMoreDetails;
        public TextView txtCompanyName;
        public LinearLayout llCompanyName;
        public TextView txtLastVisitedDate;
        public LinearLayout llHighlight;

        public void fill(final Job item, final int position) {
            txtJobTitle.setText(item.getTitle());

            StringBuilder detailText = new StringBuilder();

            detailText.append(DateUtil.getTimeAgo(item.getDate()));

            if(item.getLocationNames() != null && !item.getLocationNames().isEmpty()) {
                detailText.append(" - ");

                for(Job.LocationName locName : item.getLocationNames()) {
                    String cityName = locName.getCityName();
                    String stateName = locName.getStateName();

                    if(cityName != null) {
                        if(detailText.indexOf(cityName) < 0) {
                            detailText.append(cityName).append(context.getString(R.string.comma) + " ");
                        }
                    }
                    if(stateName != null) {
                        if(detailText.indexOf(stateName) < 0) {
                            detailText.append(stateName).append(context.getString(R.string.comma) + " ");
                        }
                    }
                }

                if(detailText.length() > 0) {
                    detailText = new StringBuilder(detailText.subSequence(0, detailText.length() -2));
                }
            }

            if(item.getCompany() != null) {
                llCompanyName.setVisibility(View.VISIBLE);
                txtCompanyName.setText(item.getCompany().getName());
                txtCompanyName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(context instanceof OnCompanySelected) {
                            ((OnCompanySelected) context).onSelectCompany(item.getCompany());
                        }
                    }
                });
            }
            else {
                llCompanyName.setVisibility(View.GONE);
            }

            txtMoreDetails.setText(detailText);
            if(detailText.equals("")) {
                txtMoreDetails.setVisibility(View.GONE);
            }

            Long visitDate = JobMgr.getInstance().getJobVisitedDate(item.getId());
            String txtVisited = "";
            if(visitDate != null) {
                rootView.setBackgroundColor(Color.parseColor("#f4f8ff"));
                txtJobTitle.setTextColor(Color.parseColor("#551a8b"));
                txtVisited = DateUtil.getTimeAgo(visitDate) + " "+ context.getString(R.string.visited);
            }
            else {
                rootView.setBackgroundColor(context.getResources().getColor(android.R.color.background_light));
                txtJobTitle.setTextColor(Color.parseColor("#0000cc"));
            }
            if(txtVisited.equals(""))
                txtLastVisitedDate.setVisibility(View.GONE);
            else {
                txtLastVisitedDate.setText(txtVisited);
                txtLastVisitedDate.setVisibility(View.VISIBLE);
            }

            final boolean isFavorite = JobMgr.getInstance().isJobFavorited(item.getId());

            if(isFavorite) {
                imgFavorite.setColorFilter(null);
            }
            else {
                imgFavorite.setColorFilter(Color.parseColor("#a9a9a9"));
            }

            final View heartIcon = rootView.findViewById(R.id.ll_favorite_job);
            heartIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isFavorite) {
                        JobMgr.getInstance().removeFromFavorite(item.getId());
                        favoritePosition = -1;
                    }
                    else {
                        JobMgr.getInstance().addJobToFavorited(item.getId());
                        item.setFavorite(false);
                    }

                    notifyDataSetChanged();

                    if(listener != null)
                        listener.onFavoriteClicked(item, position);
                }
            });

            final View favoriteView = mainView.findViewById(R.id.ll_favorite_jobs_menu);
            if(position == favoritePosition)
                favoriteView.setVisibility(View.VISIBLE);
            else
                favoriteView.setVisibility(View.GONE);

            /*
            mainView.findViewById(R.id.txt_remove_from_favorites).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    favoritePosition = -1;
                    notifyDataSetChanged();

                    heartIcon.performClick();
                }
            });
            */

            mainView.findViewById(R.id.txt_favorite_list).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, FavoriteJobsActivity.class));
                }
            });

            if(System.currentTimeMillis() - item.getDate() < (24 * 60 * 60 * 1000)) {
                txtNewJob.setVisibility(View.VISIBLE);
            }
            else {
                txtNewJob.setVisibility(View.GONE);
            }

            llHighlight.removeAllViews();

            if(item.getHighlight() != null) {
                int counter = 0;
                for (String txt : item.getHighlight()) {
                    TextView txtHighlight = (TextView) inflater.inflate(R.layout.txt_highlight, null);

                    String highlightTxt = txt.replace("start_highlight", "<b>")
                            .replace("end_highlight", "</b>") + " ";

                    txtHighlight.setText(Html.fromHtml(highlightTxt));

                    llHighlight.addView(txtHighlight);

                    counter++;
                    if(counter > 2) {
                        break;
                    }
                }
            }
        }
    }

    int favoritePosition = -1;

    public void showFavoriteOptionsAtPosition(int position) {
        favoritePosition = position;
    }

    OnFavoriteClickListener listener;

    public void setOnFavoriteClickListener(OnFavoriteClickListener listener) {
        this.listener = listener;
    }

    public interface OnFavoriteClickListener {
        void onFavoriteClicked(Job job, int position);
    }
}
