package ir.areka.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.util.Configs;

/**
 * Created by mahdi on 11/2/16.
 */

public class SettingsFragment extends Fragment {

    long aDay = 24 * 60 * 60 * 1000;
    long threeDay = 3 * 24 * 60 * 60 * 1000;
    long aWeek = 7 * 24 * 60 * 60 * 1000;

    int counter = 0;
    long lastTouchDate = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_settings, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Spinner spinner = (Spinner) getView().findViewById(R.id.spinner_check_update);

        List<String> list = new ArrayList<>();
        list.add(getString(R.string.every_day));
        list.add(getString(R.string.every_three_day));
        list.add(getString(R.string.every_week));
        list.add(getString(R.string.disable));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        long previousPeriod = Configs.getInstance(getContext()).getCheckUpdatePeriodDate();
        int selectionPosition = 0;
        if(previousPeriod == threeDay) {
            selectionPosition = 1;
        }
        else if(previousPeriod == aWeek) {
            selectionPosition = 2;
        }
        else if(previousPeriod == -1) {
            selectionPosition = 3;
        }
        spinner.setSelection(selectionPosition);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                long period = 0l;

                switch (i) {
                    case 0:
                        period = aDay;
                        break;
                    case 1:
                        period = threeDay;
                        break;
                    case 2:
                        period = aWeek;
                        break;
                    case 3:
                        period = -1;
                        break;
                }

                Configs.getInstance(getActivity()).setCheckUpdatePeriodDate(period);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(lastTouchDate < System.currentTimeMillis() - (60 * 1000)) {
                    counter = 0;
                }

                counter++;
                lastTouchDate = System.currentTimeMillis();

                if(counter == 4) {
                    Toast.makeText(getContext(), "you are developer", Toast.LENGTH_SHORT).show();
                    Configs.getInstance(getContext()).setCheckUpdatePeriodDate(5_000);
                    counter = 0;
                }

            }
        });
    }
}
