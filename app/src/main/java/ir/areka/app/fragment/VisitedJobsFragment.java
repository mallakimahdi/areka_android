package ir.areka.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.adapter.JobListAdapter;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.job.ViewedJob;
import ir.areka.app.model.PageList;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 11/16/16.
 */

public class VisitedJobsFragment extends Fragment {

    List<Job> jobs = new ArrayList<>();
    JobListAdapter adapter;
    private View txtNoVisitedItem;
    private View llResultBox;
    private View prgLoadingVisitedJobs;
    private Context context;
    private Call<PageList<Job>> visitedJobsCall;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_visited_jobs, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView lstVisitedJobs = (ListView) getView().findViewById(R.id.lst_visited_jobs);
        adapter = new JobListAdapter(getActivity(), jobs);
        lstVisitedJobs.setAdapter(adapter);
        txtNoVisitedItem = getView().findViewById(R.id.txt_no_visited_any);
        llResultBox = getView().findViewById(R.id.ll_result_box);
        prgLoadingVisitedJobs = getView().findViewById(R.id.prg_loading_visited_jobs);

        lstVisitedJobs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ((OnJobSelectedListener) context).onJobSelected(null, (Job) parent.getItemAtPosition(position));
            }
        });

        createOrRefreshView();
    }

    private void createOrRefreshView() {

        List<ViewedJob> visitedJobs = JobMgr.getInstance().getVisitedJobs();

        if(!visitedJobs.isEmpty()) {
            txtNoVisitedItem.setVisibility(View.GONE);
            llResultBox.setVisibility(View.VISIBLE);

            prgLoadingVisitedJobs.setVisibility(View.VISIBLE);

            final List<String> visitedJobsIds = new ArrayList<>();
            for(ViewedJob vj : visitedJobs) {
                if(vj.getJobId() != null) {
                    visitedJobsIds.add(vj.getJobId());
                }
            }

            visitedJobsCall = ArekaWebService.getInstance().getWebService().getJobsByIds(visitedJobsIds);
            visitedJobsCall.enqueue(new Callback<PageList<Job>>() {
                @Override
                public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {

                    PageList<Job> jobList = response.body();

                    if(jobList == null)
                        return;

                    Collections.sort(jobList.getItems(), new Comparator<Job>() {
                        public int compare(Job left, Job right) {
                            return ((Integer) visitedJobsIds.indexOf(left.getId())).compareTo(visitedJobsIds.indexOf(right.getId()));
                        }
                    });

                    jobs.clear();
                    jobs.addAll(jobList.getItems());
                    adapter.notifyDataSetChanged();

                    prgLoadingVisitedJobs.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<PageList<Job>> call, Throwable t) {
                    prgLoadingVisitedJobs.setVisibility(View.GONE);
                }
            });
        }
        else {
            txtNoVisitedItem.setVisibility(View.VISIBLE);
            llResultBox.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(visitedJobsCall != null) {
            visitedJobsCall.cancel();
        }
    }
}
