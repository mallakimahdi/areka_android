package ir.areka.app.fragment;

import ir.areka.app.model.job.Job;

/**
 * Created by mahdi on 8/14/16.
 */
public interface OnJobSelectedListener {
    void onJobSelected(String searchQuery, Job job);
}
