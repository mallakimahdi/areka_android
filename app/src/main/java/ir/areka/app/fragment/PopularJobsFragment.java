package ir.areka.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.activity.SearchResultActivity;
import ir.areka.app.adapter.PopularJobsAdapter;
import ir.areka.app.model.job.PopularJob;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 9/17/16.
 */
public class PopularJobsFragment extends Fragment {

    private Context context;
    LayoutInflater inflater;
    ListView lstPopularJobs;
    ArrayList<PopularJob> popularJobs;
    PopularJobsAdapter adapter;
    ProgressBar prgLoadingPopularJobs;
    Call<List<PopularJob>> getPopularJobsCall;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_popular_jobs, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstPopularJobs = (ListView) getView().findViewById(R.id.lst_popular_jobs);
        prgLoadingPopularJobs = (ProgressBar) getView().findViewById(R.id.prg_loading_popular_jobs);
        popularJobs = new ArrayList<>();
        adapter = new PopularJobsAdapter(getActivity(), popularJobs);
        lstPopularJobs.setAdapter(adapter);

        lstPopularJobs.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(context, SearchResultActivity.class);

                        String key = popularJobs.get(position).getKey();

                        i.putExtra("query", key);

                        startActivity(i);
                    }
                }
        );

        createOrRefreshView();
    }

    public void createOrRefreshView() {

        getPopularJobsCall = ArekaWebService.getInstance().getWebService().getPopularJobs();
        getPopularJobsCall.enqueue(new Callback<List<PopularJob>>() {
            @Override
            public void onResponse(Call<List<PopularJob>> call, Response<List<PopularJob>> response) {

                if (response.body() == null)
                    return;

                adapter.clear();

                for (PopularJob pj : response.body()) {
                    adapter.add(pj);
                }

                adapter.notifyDataSetChanged();
                prgLoadingPopularJobs.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PopularJob>> call, Throwable t) {
                prgLoadingPopularJobs.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getPopularJobsCall != null) {
            getPopularJobsCall.cancel();
        }
    }
}
