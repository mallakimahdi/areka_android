package ir.areka.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cocosw.undobar.UndoBarController;
import com.cocosw.undobar.UndoBarStyle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.adapter.JobListAdapter;
import ir.areka.app.model.job.FavoritedJob;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.PageList;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 9/23/16.
 */
public class FavoriteJobsFragment extends Fragment {
    private Context context;
    List<Job> jobList = new ArrayList<>();
    List<String> favorites = new ArrayList<>();
    private Call<PageList<Job>> getFavoriteCall;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_favorite_jobs, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createOrRefreshView();
    }

    private void createOrRefreshView() {
        favorites.clear();
        for(FavoritedJob fj : JobMgr.getInstance().getAllFavoritedJobs()) {
            favorites.add(fj.getJobId());
        }

        final ListView lstFavoriteJobs = (ListView) getView().findViewById(R.id.lst_favorite_jobs);
        final TextView txtEmptyFavorite = (TextView) getView().findViewById(R.id.txt_empty_favorite_list);

        final JobListAdapter adapter = new JobListAdapter(context, jobList);
        lstFavoriteJobs.setAdapter(adapter);

        adapter.setOnFavoriteClickListener(new JobListAdapter.OnFavoriteClickListener() {
            @Override
            public void onFavoriteClicked(final Job job, final int position) {
                jobList.remove(job);
                adapter.notifyDataSetChanged();

                new UndoBarController.UndoBar(getActivity()).message(R.string.deleted)
                        .listener(new UndoBarController.UndoListener() {
                            @Override
                            public void onUndo(@Nullable Parcelable token) {
                                jobList.add(position, job);
                                JobMgr.getInstance().addJobToFavorited(job.getId());
                                adapter.notifyDataSetChanged();
                            }
                        })
                        .style(new UndoBarStyle(
                                com.cocosw.undobar.R.drawable.ic_undobar_undo, R.string.undo_job))
                        .show();
            }
        });

        getFavoriteCall = ArekaWebService.getInstance().getWebService().getFavoriteJobs(favorites);
        getFavoriteCall.enqueue(new Callback<PageList<Job>>() {
            @Override
            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {

                PageList<Job> jobs = response.body();

                if(jobs == null || getView() == null)
                    return;

                getView().findViewById(R.id.prg_loading_favorite_jobs).setVisibility(View.GONE);

                if(jobs.getItems().isEmpty()) {
                    txtEmptyFavorite.setVisibility(View.VISIBLE);
                    lstFavoriteJobs.setVisibility(View.GONE);

                    return;
                }

                List<Job> items = jobs.getItems();

                Collections.sort(items, new Comparator<Job>() {
                    @Override
                    public int compare(Job left, Job right) {
                        return Integer.valueOf(favorites.indexOf(left.getId())).compareTo(favorites.indexOf(right.getId()));
                    }
                });

                jobList.clear();
                jobList.addAll(items);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PageList<Job>> call, Throwable t) {
                if(getView() == null) {
                    return;
                }

                getView().findViewById(R.id.prg_loading_favorite_jobs).setVisibility(View.GONE);
            }
        });

        lstFavoriteJobs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((OnJobSelectedListener) context).onJobSelected(null, (Job) parent.getItemAtPosition(position));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getFavoriteCall != null) {
            getFavoriteCall.cancel();
        }
    }
}
