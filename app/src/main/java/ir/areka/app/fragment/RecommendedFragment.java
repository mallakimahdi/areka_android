package ir.areka.app.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ir.areka.app.R;
import ir.areka.app.adapter.JobListAdapter;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.PageList;
import ir.areka.app.model.search.SearchHistory;
import ir.areka.app.model.search.SearchMgr;
import ir.areka.app.net.ArekaWebService;
import ir.areka.app.util.WordNormalizer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 11/28/16.
 */

public class RecommendedFragment extends Fragment {

    private Context context;
    private LayoutInflater inflater;

    View rowPrgCommentWaiting;
    View rowRetryConnectionComment;
    int lastLoadedItem;
    int totalLoadedItem;
    int completeLoadedPart;

    List<Job> jobList = new ArrayList<>();
    private TextView searchResultCount;
    private JobListAdapter jobListAdapter;
    private String searchText;
    private String locationId;
    private long lastSearchDateToServer;
    private ListView lstRecommendedJobs;
    private Call<PageList<Job>> getRecommendedCall;

    int retryCount = 0;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_recommended_jobs, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstRecommendedJobs = (ListView) getView().findViewById(R.id.lst_recommended_jobs);
        View totalCountView = inflater.inflate(R.layout.view_total_count, null);
        lstRecommendedJobs.addHeaderView(totalCountView);

        rowPrgCommentWaiting = LayoutInflater.from(context).inflate(R.layout.row_prg_waiting, null);
        rowRetryConnectionComment = LayoutInflater.from(context).inflate(R.layout.view_error_connection, null);
        lstRecommendedJobs.addFooterView(rowPrgCommentWaiting);
        lstRecommendedJobs.addFooterView(rowRetryConnectionComment);

        searchResultCount = (TextView) totalCountView.findViewById(R.id.txt_total_search_count);
        jobListAdapter = new JobListAdapter(context, jobList);
        lstRecommendedJobs.setAdapter(jobListAdapter);

        lstRecommendedJobs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ((OnJobSelectedListener) context).onJobSelected(null, (Job) adapterView.getItemAtPosition(i));

            }
        });

        jobListAdapter.setOnFavoriteClickListener(new JobListAdapter.OnFavoriteClickListener() {
            @Override
            public void onFavoriteClicked(Job job, int position) {

                boolean isFavorite = JobMgr.getInstance().isJobFavorited(jobListAdapter.getItem(position).getId());
                if(isFavorite) {
                    jobListAdapter.showFavoriteOptionsAtPosition(position);
                    lstRecommendedJobs.getChildAt(position - lstRecommendedJobs.getFirstVisiblePosition() + 1)
                            .findViewById(R.id.ll_favorite_jobs_menu).setVisibility(View.VISIBLE);
                }
            }
        });

        List<SearchHistory> historySearches = SearchMgr.getInstance().getSearchHistories();
        if(historySearches.size() < 1) {
            getView().findViewById(R.id.txt_no_recommended).setVisibility(View.VISIBLE);
            return;
        }

        Set<String> uniqueQueries = new HashSet<>();
        Set<Integer> uniqueLocations = new HashSet<>();

        for(SearchHistory sh : historySearches) {
            uniqueQueries.add(sh.getQuery().trim());
            uniqueLocations.addAll(sh.getLocationIdsArray());
        }

        searchText = StringUtils.join(uniqueQueries, ",");
        locationId = StringUtils.join(uniqueLocations, ",");

        lastSearchDateToServer = JobMgr.getInstance().getLastVisitedRecommended();

        JobMgr.getInstance().setLastVisitedRecommendedJobs(System.currentTimeMillis());

        createOrRefreshView();
    }

    private void createOrRefreshView() {
        jobList.clear();
        lastLoadedItem = 0;
        totalLoadedItem = 0;
        completeLoadedPart = 0;

        lstRecommendedJobs.setOnScrollListener(listScrollListener);
        jobListAdapter.notifyDataSetChanged();
    }

    AbsListView.OnScrollListener listScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            lastLoadedItem = firstVisibleItem + visibleItemCount;
            totalLoadedItem = totalItemCount;

            downloadResultPart();
        }
    };

    private void downloadResultPart() {
        if (lastLoadedItem == totalLoadedItem) {
            int currentPart = (int) (Math.ceil((totalLoadedItem - 2) / 20) - 1);
            int requestPart = currentPart + 1;
            if (requestPart >= completeLoadedPart) {
                addOrRefreshSearchResult(requestPart);
                rowPrgCommentWaiting.setVisibility(View.VISIBLE);
                rowRetryConnectionComment.setVisibility(View.INVISIBLE);
                completeLoadedPart++;
            }
        }
    }

    private void addOrRefreshSearchResult(final int requestPart) {

        Map<String, String> queries = new HashMap<>();

        if (searchText != null) {
            queries.put("q", searchText);
        }

        if (locationId != null) {
            queries.put("locationId", locationId);
        }

        queries.put("page", requestPart+"");

        /*
        if (lastSearchDateToServer != 0l) {
            long yesterdayDate = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
            if(lastSearchDateToServer > yesterdayDate) {
                lastSearchDateToServer = yesterdayDate;
            }
        }
        */

        queries.put("lastSearchDate", lastSearchDateToServer+"");

        queries.put("searchMode", "recommended");

        getRecommendedCall = ArekaWebService.getInstance().getWebService().searchJobs(queries);
        getRecommendedCall.enqueue(new Callback<PageList<Job>>() {
            @Override
            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {

                PageList<Job> jobs = response.body();
                if(jobs == null || jobs.getItems() == null)
                    return;

                if(!isAdded())
                    return;

                rowPrgCommentWaiting.setVisibility(View.GONE);

                if (retryCount < 1 && jobs.getItems().isEmpty() && jobList.isEmpty()) {
                    lastSearchDateToServer = 0l;
                    retryCount++;
                    createOrRefreshView();
                }

                String totalCount = WordNormalizer.getInstance().toArabicDigits(jobs.getTotalCount());
                searchResultCount.setText(totalCount);
                jobList.addAll(jobs.getItems());
                jobListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PageList<Job>> call, Throwable t) {
                completeLoadedPart--;
                rowPrgCommentWaiting.setVisibility(View.INVISIBLE);
                rowRetryConnectionComment.setVisibility(View.VISIBLE);
                ImageView btnError = (ImageView) rowRetryConnectionComment.findViewById(R.id.btn_error);
                btnError.setColorFilter(Color.parseColor("#a9a9a9"));
                btnError.setOnClickListener(clickRetryGetComments);

            }
        });
    }

    View.OnClickListener clickRetryGetComments = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rowRetryConnectionComment.setVisibility(View.INVISIBLE);
            downloadResultPart();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getRecommendedCall != null) {
            getRecommendedCall.cancel();
        }
    }
}
