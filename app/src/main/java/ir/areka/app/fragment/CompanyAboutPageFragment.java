package ir.areka.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ir.areka.app.R;
import ir.areka.app.model.company.Company;
import ir.areka.app.model.location.GeoLocation;

/**
 * Created by mahdi on 11/11/16.
 */

public class CompanyAboutPageFragment extends Fragment implements OnMapReadyCallback {

    private Company company;
    private GeoLocation geoLocation;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_company_about, container, false);
        return rootView;
    }

    private void createOrRefreshView() {
        if(company == null || getView() == null) {
            return;
        }

        if (company.getGeoLocations() != null && !company.getGeoLocations().isEmpty()) {
            geoLocation = company.getGeoLocations().get(0);

            getView().findViewById(R.id.ll_company_map).setVisibility(View.VISIBLE);

            SupportMapFragment fragment = new SupportMapFragment();
            getChildFragmentManager().beginTransaction().add(R.id.frg_company_map, fragment).commit();
            fragment.getMapAsync(this);
        }

        StringBuilder aboutText = new StringBuilder();

        if(company.getDescription() != null && !company.getDescription().isEmpty()) {
            for(String desc : company.getDescription()) {
                aboutText.append(desc.replaceAll("(?m)^[ \t]*\r?\n", "").trim()).append("\n");
            }
            aboutText.append("\n");
        }

        if(company.getTags() != null && !company.getTags().isEmpty()) {
            aboutText.append(getString(R.string.active_state) +": ");

            for(String tag : company.getTags()) {
                aboutText.append(tag).append(getString(R.string.comma));
            }

            aboutText.deleteCharAt(aboutText.length() - 1);
            aboutText.append("\n"+"\n");
        }

        if(company.getAddress() != null) {
            aboutText.append(getString(R.string.address)+": ").append(company.getAddress()).append("\n"+"\n");
        }

        if(company.getWebsites() != null && !company.getWebsites().isEmpty()) {
            aboutText.append(getString(R.string.website) + ": ");

            for(String website : company.getWebsites()) {
                aboutText.append(website).append(getString(R.string.comma));
            }

            aboutText.deleteCharAt(aboutText.length() - 1);
            aboutText.append("\n");
        }

        ((TextView) getView().findViewById(R.id.txt_company_about)).setText(aboutText.toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        createOrRefreshView();
    }

    public Fragment setCompany(Company company) {
        this.company = company;
        createOrRefreshView();
        return this;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(geoLocation != null) {
            LatLng latLng = new LatLng(geoLocation.getLat(), geoLocation.getLon());

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));

            googleMap.getUiSettings().setMapToolbarEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);

            googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(company.getName())
            ).showInfoWindow();
        }
    }
}
