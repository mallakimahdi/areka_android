package ir.areka.app.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ir.areka.app.R;
import ir.areka.app.activity.App;
import ir.areka.app.activity.BaseActivity;
import ir.areka.app.activity.PopularJobsActivity;
import ir.areka.app.activity.RecommendedJobsActivity;
import ir.areka.app.activity.SearchResultActivity;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.job.NewJobsDTO;
import ir.areka.app.model.PageList;
import ir.areka.app.model.location.UserCity;
import ir.areka.app.model.search.SearchHistory;
import ir.areka.app.model.search.SearchMgr;
import ir.areka.app.net.ArekaWebService;
import ir.areka.app.util.LocationUtil;
import ir.areka.app.util.WordNormalizer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback {

    private Context context;
    private EditText edtSearchInput;
    private View llLogo;
    private LinearLayout llMainHome;
    private Integer locationId;
    private String locationName;
    private Button btnSearch;
    private ProgressBar prgFindingLocation;

    final int REQUEST_FINE_LOCATION_PERMISSION = 1234;
    final int REQUEST_ENABLE_LOCATION_PERMISSION = 1235;
    private Activity activity;
    private View clearAllSearchHistories;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_home, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        llMainHome = (LinearLayout) getView().findViewById(R.id.ll_main_home);
        edtSearchInput = (EditText) getView().findViewById(R.id.edt_search_input);
        llLogo = getView().findViewById(R.id.ll_logo);
        btnSearch = (Button) getView().findViewById(R.id.btn_search);
        prgFindingLocation = (ProgressBar) getView().findViewById(R.id.prg_finding_location);
        clearAllSearchHistories = getView().findViewById(R.id.txt_clear_all_search_histories);

        clearAllSearchHistories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchMgr.getInstance().deleteAll();
                createOrRefreshView();
            }
        });

        edtSearchInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocused) {
                if (isFocused) {
                    llLogo.setVisibility(View.GONE);

                    llMainHome.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT
                            , FrameLayout.LayoutParams.MATCH_PARENT, Gravity.TOP));
                }
            }
        });
        edtSearchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if(actionId == EditorInfo.IME_ACTION_SEARCH) {
                    findLocationAndSearch();
                    return true;
                }

                return false;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findLocationAndSearch();
            }
        });
    }

    private void showRecommendedJobs() {
        final View recommendedJobsView = getView().findViewById(R.id.recommended_jobs);
        final View recommendedTextProgressing = recommendedJobsView.findViewById(R.id.txt_progressing);
        final TextView txtNewRecommendJobCount = (TextView) recommendedJobsView.findViewById(R.id.txt_new_job_count);
        TextView txtRecommendedSearchQuery = (TextView) recommendedJobsView.findViewById(R.id.txt_search_query);

        List<SearchHistory> historySearches = SearchMgr.getInstance().getSearchHistories();
        if(historySearches.size() < 2) {
            recommendedJobsView.setVisibility(View.GONE);
            return;
        }
        else {
            recommendedJobsView.setVisibility(View.VISIBLE);
        }

        txtRecommendedSearchQuery.setText(getString(R.string.recommended_jobs));

        Set<String> uniqueQueries = new HashSet<>();
        Set<Integer> uniqueLocations = new HashSet<>();

        for(SearchHistory sh : historySearches) {
            uniqueQueries.add(sh.getQuery().trim());
            uniqueLocations.addAll(sh.getLocationIdsArray());
        }
        uniqueLocations.remove(0);

        final String queries = StringUtils.join(uniqueQueries, ",");
        final String locations = StringUtils.join(uniqueLocations, ",");

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("q", queries);
        queryParams.put("locationId", locations);
        queryParams.put("searchMode", "recommended");
        queryParams.put("pageSize", 0+"");

        long lastVisitedDate = JobMgr.getInstance().getLastVisitedRecommended();
        if(lastVisitedDate != 0l) {
            queryParams.put("lastSearchDate", lastVisitedDate + "");
        }

        ArekaWebService.getInstance().getWebService().searchJobs(queryParams).enqueue(new Callback<PageList<Job>>() {
            @Override
            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {

                if(getView() == null || response == null || response.body() == null)
                    return;

                recommendedTextProgressing.setVisibility(View.GONE);

                int totalCount = response.body().getTotalCount();
                if(totalCount > 0) {
                    txtNewRecommendJobCount.setText(totalCount + " " + getString(R.string.new_jobs));
                }
                else {
                    txtNewRecommendJobCount.setText("");
                }
            }

            @Override
            public void onFailure(Call<PageList<Job>> call, Throwable t) {
                recommendedTextProgressing.setVisibility(View.GONE);
            }
        });

        recommendedJobsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(), RecommendedJobsActivity.class);
                i.putExtra("searchText", queries);
                i.putExtra("locationId", locations);

                startActivity(i);
            }
        });
    }

    private void showSearchHistory() {

        final LinearLayout rootView = (LinearLayout) getView().findViewById(R.id.ll_search_history);
        View searchHistoryBox = getView().findViewById(R.id.ll_search_history_box);
        View allNewJobsBox = getView().findViewById(R.id.ll_all_new_jobs);

        rootView.removeAllViews();

        List<SearchHistory> searchHistories = SearchMgr.getInstance().getSearchHistories();

        if (!searchHistories.isEmpty()) {
            allNewJobsBox.setVisibility(View.GONE);
            searchHistoryBox.setVisibility(View.VISIBLE);

            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.gravity = Gravity.TOP;
            llMainHome.setLayoutParams(layoutParams);
        } else {

            searchHistoryBox.setVisibility(View.GONE);
            allNewJobsBox.setVisibility(View.VISIBLE);
            llLogo.setVisibility(View.VISIBLE);

            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.gravity = Gravity.CENTER;
            llMainHome.setLayoutParams(layoutParams);

            final TextView txtNewJobs = (TextView) getView().findViewById(R.id.txt_total_count_new_jobs);

            ArekaWebService.getInstance().getWebService().getNewJobsLastWeek().enqueue(new Callback<NewJobsDTO>() {
                @Override
                public void onResponse(Call<NewJobsDTO> call, Response<NewJobsDTO> response) {

                    if(getView() == null || response == null || response.body() == null)
                        return;

                    getView().findViewById(R.id.txt_new_jobs).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(getActivity(), PopularJobsActivity.class));
                        }
                    });

                    txtNewJobs.setText(WordNormalizer.getInstance().toArabicDigits(response.body().getCount()));
                }

                @Override
                public void onFailure(Call<NewJobsDTO> call, Throwable t) {
                    txtNewJobs.setVisibility(View.GONE);
                }
            });
        }

        ArekaWebService.getInstance().getWebService().getSearchHistory(searchHistories).enqueue(new Callback<List<Integer>>() {
            @Override
            public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {

                List<Integer> results = response.body();

                if(results == null || getView() == null)
                    return;

                for (int i = 0; i < rootView.getChildCount(); i++) {

                    rootView.getChildAt(i).findViewById(R.id.txt_progressing).setVisibility(View.GONE);
                    if (results.get(i) > 0) {
                        ((TextView) rootView.getChildAt(i).findViewById(R.id.txt_new_job_count))
                                .setText(results.get(i) + " " + getString(R.string.new_jobs));
                        rootView.getChildAt(i).findViewById(R.id.txt_dash).setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Integer>> call, Throwable t) {
                for (int i = 0; i < rootView.getChildCount(); i++) {
                    rootView.getChildAt(i).findViewById(R.id.txt_progressing).setVisibility(View.GONE);
                }
            }
        });

        for (final SearchHistory history : searchHistories) {
            View view = LayoutInflater.from(context).inflate(R.layout.row_search_recommend, null);

            String text = history.getQuery();
            if (history.getLocationName() != null && !history.getLocationName().trim().equals(""))
                text += " - " + history.getLocationName();

            ((TextView) view.findViewById(R.id.txt_search_query)).setText(text);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(context, SearchResultActivity.class);
                    i.putExtra("query", history.getQuery());
                    i.putExtra("locationId", StringUtils.join(history.getLocationIds(), ",") );
                    i.putExtra("locationName", history.getLocationName());

                    long yesterdayDate = System.currentTimeMillis() - 24 * 60 * 60 * 1000;
                    long date = history.getLastUpdateDate();
                    if(history.getLastUpdateDate() > yesterdayDate) {
                        date = yesterdayDate;
                    }
                    i.putExtra("lastSearchDate", date);

                    startActivity(i);
                }
            });

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.dialog_delete_search_history);
                    dialog.findViewById(R.id.ll_delete_search_history).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchMgr.getInstance().deleteSearchQuery(history);
                            dialog.dismiss();
                            createOrRefreshView();
                        }
                    });
                    dialog.show();

                    return false;
                }
            });

            rootView.addView(view);
        }
    }

    private void findLocationAndSearch() {
        prgFindingLocation.setVisibility(View.VISIBLE);

        UserCity userLocation = LocationUtil.getLatestUserLocation();

        if(userLocation != null && userLocation.getDate() + (7 * 24 * 60 * 60 * 1000) > System.currentTimeMillis() ) {
            locationId = userLocation.getId();
            locationName = userLocation.getCity();
            doSearch();

            return;
        }

        int permissionCheck = ContextCompat.checkSelfPermission(App.getContext()
                , Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}
                    , REQUEST_FINE_LOCATION_PERMISSION);
        }
        else {
            turnOnGps();
        }

        btnSearch.clearFocus();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION_PERMISSION:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    turnOnGps();
                }
                else {
                    doSearch();
                }
                break;
            default:
                break;
        }
    }

    public void turnOnGps() {

        if(!LocationUtil.isNetworkProviderExists()) {
            doSearch();
            return;
        }

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(App.getContext())
                    .addApi(LocationServices.API).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(10000 / 2);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            getLocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            if(getActivity() != null) {
                                try {
                                    status.startResolutionForResult(getActivity(), REQUEST_ENABLE_LOCATION_PERMISSION);
                                }
                                catch (IntentSender.SendIntentException e) {
                                    Log.e("enabling gps", "can't enable gps");
                                }
                            }
                            else {
                                doSearch();
                            }
                            break;
                        default:
                            doSearch();
                            break;
                    }
                }
            });
        }
        else {
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);

            getLocation();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_ENABLE_LOCATION_PERMISSION) {
            if (resultCode == 0) {
                doSearch();
            }
            else {
                getLocation();
            }
        }
    }

    private void doSearch() {

        prgFindingLocation.setVisibility(View.INVISIBLE);

        String query = edtSearchInput.getText().toString();

        if (query == null || query.equals("")) {
            if(getView() != null) {
                Toast.makeText(getContext(), getString(R.string.please_insert_job_name)
                        , Toast.LENGTH_LONG).show();
            }
            return;
        }

        Intent i = new Intent(context, SearchResultActivity.class);
        i.putExtra("query", query);
        i.putExtra("locationName", locationName);
        i.putExtra("locationId", locationId+"");

        if(getView() != null) {
            startActivity(i);
        }
    }

    public void setContextActivity(Activity activity) {
        this.activity = activity;
    }

    private void getLocation() {
        LocationUtil.getLocation(new LocationUtil.LocationReceivedListener() {
            @Override
            public void locationReceived(Integer locId, String locName) {
                locationId = locId;
                locationName = locName;

                doSearch();
            }

            @Override
            public void onTimeOutGetLocation() {
                doSearch();
            }
        });
    }

    private void createOrRefreshView() {
        showSearchHistory();
        showRecommendedJobs();
    }

    @Override
    public void onResume() {
        super.onResume();
        createOrRefreshView();
        ((BaseActivity) getActivity()).showActionbar();
    }
}
