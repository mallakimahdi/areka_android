package ir.areka.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ir.areka.app.R;
import ir.areka.app.activity.App;
import ir.areka.app.activity.FavoriteJobsActivity;
import ir.areka.app.activity.WebViewActivity;
import ir.areka.app.adapter.JobListAdapter;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.PageList;
import ir.areka.app.model.location.GeoLocation;
import ir.areka.app.net.ArekaWebService;
import ir.areka.app.util.ShareUtil;
import ir.areka.app.util.DateUtil;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 8/13/16.
 */
public class JobPreviewFragment extends Fragment {

    private Job job;
    private Context context;
    private boolean isJobFavorite;
    private ImageView btnFavorite;
    private View favoriteJobsOperationView;
    private ProgressBar prgLoadingSimilarJobs;
    private Call<PageList<Job>> getSimilarJobsCall;
    private String searchQuery;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_job_preview, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (job == null) {
            return;
        }

        getSimilarJobsCall = ArekaWebService.getInstance().getWebService().getSimilarResult(job.getId());

        ((TextView) getView().findViewById(R.id.txt_dialog_job_title)).setText(job.getTitle());

        View llCompanyPage = getView().findViewById(R.id.ll_company_page);
        if (job.getCompany() != null && job.getCompany().getId() != null) {

            TextView txtCompanyName = (TextView) getView().findViewById(R.id.txt_company_name);
            txtCompanyName.setText(job.getCompany().getName());

            ((ImageView) getView().findViewById(R.id.img_show_company_detail)).setColorFilter(getResources().getColor(R.color.more_details_flag));

            llCompanyPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (context instanceof OnCompanySelected) {
                        ((OnCompanySelected) context).onSelectCompany(job.getCompany());
                    }

                    interactWithJob("companyClick");
                }
            });
        } else {
            llCompanyPage.setVisibility(View.GONE);
        }

        String detailText = "";
        if (job.getLocationNames() != null) {
            for (Job.LocationName locName : job.getLocationNames()) {
                if (locName.getCityName() != null) {
                    detailText += locName.getCityName() + context.getString(R.string.comma) + " ";
                }
                if (locName.getStateName() != null) {
                    detailText += locName.getStateName() + ", ";
                }
            }
        }
        if (detailText.length() > 0) {
            detailText = detailText.substring(0, detailText.length() - 2);
        }

        ((TextView) getView().findViewById(R.id.txt_location_name)).setText(detailText);

        StringBuilder builder = new StringBuilder();
        for (String body : job.getBody()) {
            builder.append(body).append("\n\n");
        }

        LinearLayout contactBox = (LinearLayout) getView().findViewById(R.id.contact_box);

        if (job.getPhoneNumber() != null && !job.getPhoneNumber().isEmpty()) {

            View contactView = LayoutInflater.from(context).inflate(R.layout.contact_view, null);

            LinearLayout contactLayout = (LinearLayout) contactView.findViewById(R.id.ll_contact_items);

            ((TextView) contactView.findViewById(R.id.txt_contact_way)).setText(getString(R.string.tell));

            for (final String phoneNumber : job.getPhoneNumber()) {

                TextView txtPhoneNumber = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.txt_contact_view, null);

                SpannableString content = new SpannableString(phoneNumber);
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                txtPhoneNumber.setText(content);
                txtPhoneNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                        startActivity(intent);
                        interactWithJob("phoneClick");
                    }
                });

                contactLayout.addView(txtPhoneNumber);
            }

            contactBox.addView(contactView);
        }

        if (job.getEmail() != null && !job.getEmail().isEmpty()) {

            View contactView = LayoutInflater.from(context).inflate(R.layout.contact_view, null);

            ((TextView) contactView.findViewById(R.id.txt_contact_way)).setText(getString(R.string.email));

            LinearLayout contactLayout = (LinearLayout) contactView.findViewById(R.id.ll_contact_items);

            for (final String email : job.getEmail()) {

                TextView btnSendEmail = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.txt_contact_view, null);

                String e = email.toLowerCase(Locale.ENGLISH);
                SpannableString content = new SpannableString(e);
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

                btnSendEmail.setText(content);

                btnSendEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
                        startActivity(intent);
                        interactWithJob("emailClick");
                    }
                });

                contactLayout.addView(btnSendEmail);
            }

            contactBox.addView(contactView);
        }

        if ((job.getPhoneNumber() == null || job.getPhoneNumber().isEmpty()) && (job.getEmail() == null || job.getEmail().isEmpty())) {
            TextView btn = (TextView) LayoutInflater.from(context).inflate(R.layout.txt_contact_view, null);

            String sourceDomain = "";
            try {
                sourceDomain = new URL(job.getSourceCrawle()).getHost();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            btn.setText(getString(R.string.show_original_contacts) + " " + sourceDomain);
            btn.setTransformationMethod(null);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra("url", job.getSourceCrawle());
                    startActivity(intent);
                    interactWithJob("sourceClick");
                }
            });
            contactBox.addView(btn);
        }

        ((TextView) getView().findViewById(R.id.txt_job_body)).setText(builder.toString());

        if (job.getDate() != 0)
            ((TextView) getView().findViewById(R.id.txt_dialog_job_date))
                    .setText(" - " + DateUtil.getTimeAgo(job.getDate()));

        if (job.getGeoLocations() != null && !job.getGeoLocations().isEmpty()) {

            getView().findViewById(R.id.ll_job_map).setVisibility(View.VISIBLE);

            final GeoLocation geoPoint = job.getGeoLocations().get(0);

            String txt;
            if (geoPoint.getRadius() > 0) {
                txt = getString(R.string.job_location_approximately);
            } else {
                txt = getString(R.string.job_location);
            }

            ((TextView) getView().findViewById(R.id.txt_job_location_on_map)).setText(txt);

            SupportMapFragment fragment = new SupportMapFragment();

            getChildFragmentManager().beginTransaction().add(R.id.frg_job_map, fragment).commit();
            fragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    LatLng latLng = new LatLng(geoPoint.getLat(), geoPoint.getLon());

                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

                    googleMap.getUiSettings().setMapToolbarEnabled(true);
                    googleMap.getUiSettings().setZoomControlsEnabled(true);

                    googleMap.addCircle(new CircleOptions().center(latLng)
                            .radius(geoPoint.getRadius() > 2000 ? 2000 : geoPoint.getRadius())
                            .fillColor(Color.parseColor("#30FF0000"))
                            .strokeColor(Color.TRANSPARENT)
                            .strokeWidth(0));
                }
            });
        }

        prgLoadingSimilarJobs = (ProgressBar) getView().findViewById(R.id.prg_loading_similar_jobs);

        getSimilarJobsCall.enqueue(new Callback<PageList<Job>>() {
            @Override
            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {

                final PageList<Job> jobs = response.body();

                if (getView() == null || jobs == null || jobs.getItems() == null) {
                    return;
                }

                ListView layoutSimilarityJob = (ListView) getView().findViewById(R.id.ll_similar_jobs);
                if(layoutSimilarityJob == null) {
                    return;
                }

                JobListAdapter adapter = new JobListAdapter(getActivity(), jobs.getItems());
                layoutSimilarityJob.setAdapter(adapter);
                layoutSimilarityJob.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ((OnJobSelectedListener) context).onJobSelected(searchQuery, (Job) parent.getItemAtPosition(position));
                    }
                });
                prgLoadingSimilarJobs.setVisibility(View.GONE);

                setListViewHeightBasedOnChildren(layoutSimilarityJob);
            }

            @Override
            public void onFailure(Call<PageList<Job>> call, Throwable t) {
            }
        });

        getView().findViewById(R.id.img_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, ShareUtil.shareText(context, job));
                startActivity(Intent.createChooser(sharingIntent,"Share with"));
                interactWithJob("shareClick");
            }
        });

        btnFavorite = (ImageView) getView().findViewById(R.id.btn_add_to_favorite);
        favoriteJobsOperationView = getView().findViewById(R.id.ll_favorite_jobs_menu);
        isJobFavorite = JobMgr.getInstance().isJobFavorited(job.getId());
        initOrRefreshFavoriteButton();
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOrRemoveFromFavorite();
                initOrRefreshFavoriteButton();
                interactWithJob("favoriteClick");
            }
        });

        getView().findViewById(R.id.txt_favorite_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, FavoriteJobsActivity.class));
            }
        });

        try {
            URL url = new URL(job.getSourceCrawle());
            TextView txtUrl = ((TextView) getView().findViewById(R.id.txt_source_crawl));

            txtUrl.setClickable(true);
            txtUrl.setMovementMethod(LinkMovementMethod.getInstance());
            txtUrl.setText(url.getHost());

            txtUrl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), WebViewActivity.class);
                    i.putExtra("url", job.getSourceCrawle());
                    startActivity(i);
                    interactWithJob("sourceClick");
                }
            });
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        interactWithJob("viewJob");
    }

    private void addOrRemoveFromFavorite() {

        if (isJobFavorite)
            JobMgr.getInstance().removeFromFavorite(job.getId());
        else
            JobMgr.getInstance().addJobToFavorited(job.getId());

        isJobFavorite = !isJobFavorite;
    }

    boolean isFirstTimeShowFavoriteOperations;

    private void initOrRefreshFavoriteButton() {

        int favoriteOptionVisibility = View.GONE;

        if (isJobFavorite) {
            btnFavorite.setImageResource(R.drawable.ic_action_favorite);

            if (isFirstTimeShowFavoriteOperations)
                favoriteOptionVisibility = View.VISIBLE;
        } else {
            btnFavorite.setImageResource(R.drawable.ic_action_favorite_border);
        }

        isFirstTimeShowFavoriteOperations = true;

        favoriteJobsOperationView.setVisibility(favoriteOptionVisibility);
    }

    public JobPreviewFragment setJob(Job job) {
        this.job = job;
        return this;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getSimilarJobsCall != null) {
            getSimilarJobsCall.cancel();
        }
    }

    /*
    private void visitedJobByUser() {
        Map<String, String> visitedJobMap = new HashMap<>();
        visitedJobMap.put("jobId", job.getId());
        ArekaWebService.getInstance().getWebService().visitedJob(visitedJobMap).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    */

    public JobPreviewFragment setSearchQuery(String query) {
        this.searchQuery = query;
        return this;
    }

    private void interactWithJob(String action) {

        if(job == null || job.getId() == null || searchQuery == null) {
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put("action", action);
        params.put("jobId", job.getId());
        params.put("searchQuery", searchQuery);
        params.put("interactWithJob", "");

        ArekaWebService.getInstance().getWebService().interactWithJob(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}
