package ir.areka.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.adapter.CommentListAdapter;
import ir.areka.app.dialog.AddCommentDialog;
import ir.areka.app.model.comment.Comment;
import ir.areka.app.model.comment.CommentDTO;
import ir.areka.app.model.company.Company;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 11/11/16.
 */

public class CompanyCommentPageFragment extends Fragment {

    Company company;
    private CommentListAdapter commentAdapter;
    private List<Comment> comments;
    private ListView lstComment;
    private LayoutInflater inflater;
    private View rowPrgCommentWaiting;
    private View rowRetryConnectionComment;
    private FloatingActionButton btnAddComment;
    private int lastCommentItem;
    private int totalCommentItem;
    private int completeCommentPart;
    private ViewGroup lstCommentBox;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_company_comment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        inflater = LayoutInflater.from(getActivity());

        lstComment = (ListView) getView().findViewById(R.id.lst_company_comment);
        btnAddComment = (FloatingActionButton) getView().findViewById(R.id.btn_add_comment);
        lstCommentBox = (ViewGroup) getView().findViewById(R.id.lst_comment_box);

        comments = new ArrayList<>();
        commentAdapter = new CommentListAdapter(getActivity(), comments);

        rowPrgCommentWaiting = inflater.inflate(R.layout.row_prg_waiting, null);
        rowRetryConnectionComment = inflater.inflate(R.layout.view_error_connection, null);

        lstComment.addFooterView(rowPrgCommentWaiting);
        lstComment.addFooterView(rowRetryConnectionComment);
        lstComment.setAdapter(commentAdapter);
        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddCommentDialog d = new AddCommentDialog(getActivity(), company);
                d.create();
            }
        });
        btnAddComment.attachToListView(lstComment);
        lstComment.setOnScrollListener(commentScrollListener);
    }

    AbsListView.OnScrollListener commentScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            lastCommentItem = firstVisibleItem + visibleItemCount;
            CompanyCommentPageFragment.this.totalCommentItem = totalItemCount;

            downloadCommentPart();
        }
    };

    private void downloadCommentPart() {
        if (lastCommentItem == totalCommentItem) {
            int currentPart = (int) (Math.ceil((totalCommentItem - 2) / 10) - 1);
            int requestPart = currentPart + 1;
            if (requestPart >= completeCommentPart) {
                ArekaWebService.getInstance().getWebService().getComments(company.getId(), requestPart).enqueue(downloadCommentListener);
                rowPrgCommentWaiting.setVisibility(View.VISIBLE);
                rowRetryConnectionComment.setVisibility(View.INVISIBLE);
                completeCommentPart++;
            }
        }
    }

    Callback<List<CommentDTO>> downloadCommentListener = new Callback<List<CommentDTO>>() {
        @Override
        public void onResponse(Call<List<CommentDTO>> call, Response<List<CommentDTO>> response) {

            List<CommentDTO> cDtos = response.body();

            if(cDtos == null)
                return;

            for(CommentDTO cDto : cDtos) {
                Comment c = new Comment();
                cDto.saveTo(c);
                comments.add(c);
            }

            commentAdapter.notifyDataSetChanged();

            if (comments.size() == 0) {
                lstCommentBox.removeAllViews();
                inflater.inflate(R.layout.no_comment_view, lstCommentBox);
            }
        }

        @Override
        public void onFailure(Call<List<CommentDTO>> call, Throwable t) {
            completeCommentPart--;
            rowPrgCommentWaiting.setVisibility(View.INVISIBLE);
            rowRetryConnectionComment.setVisibility(View.VISIBLE);
            rowRetryConnectionComment.findViewById(R.id.btn_error)
                    .setOnClickListener(clickRetryGetComments);
        }
    };

    View.OnClickListener clickRetryGetComments = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rowRetryConnectionComment.setVisibility(View.INVISIBLE);
            downloadCommentPart();
        }
    };

    public void setCompany(Company company) {
        this.company = company;
    }
}
