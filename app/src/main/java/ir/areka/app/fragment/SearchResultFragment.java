package ir.areka.app.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuItem;
import com.shehabic.droppy.DroppyMenuPopup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.areka.app.R;
import ir.areka.app.adapter.JobListAdapter;
import ir.areka.app.custome_view.AreaNameSuggestionCompleteView;
import ir.areka.app.custome_view.JobSuggestionCompleteView;
import ir.areka.app.custome_view.SuggestionAutoCompleteView;
import ir.areka.app.model.PageList;
import ir.areka.app.model.job.FilterJobType;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.job.JobType;
import ir.areka.app.model.location.FilterLocation;
import ir.areka.app.model.location.GeoLocation;
import ir.areka.app.model.search.SearchHistory;
import ir.areka.app.model.search.SearchMgr;
import ir.areka.app.model.suggestion.AreaNameSuggestion;
import ir.areka.app.model.suggestion.Suggestion;
import ir.areka.app.net.ArekaWebService;
import ir.areka.app.util.DateUtil;
import ir.areka.app.util.WordNormalizer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 7/29/16.
 */
public class SearchResultFragment extends Fragment {

    Integer selectedDistance;
    GeoLocation selectedGeoLocation;
    ListView lstSearchResult;
    JobListAdapter jobListAdapter;

    JobType selectedJobType;

    FilterLocation primaryFilterLocation;
    FilterLocation secondFilterLocation;
    FilterLocation selectedFilterLocation;

    List<Job> jobList = new ArrayList<>();
    String searchText;
    //List<Integer> locationIds = new ArrayList<>();

    View rowPrgCommentWaiting;
    View rowRetryConnectionComment;
    int lastLoadedItem;
    int totalLoadedItem;
    int completeLoadedPart;

    private Context context;

    private LinearLayout llCorrectQuery;

    String sortType = "relevance";
    TextView txtLocationName;
    ProgressBar prgDetectLocation;
    View searchBoxHeader;
    Long lastSearchDate;
    private TextView txtCorrectQuery;

    TextView spnJobType;
    TextView spnJobLocation;

    private List<String> locNames;

    private List<FilterLocation> filterLocations;

    //private TextView txtJobSearchAndSuggestion;
    private LayoutInflater inflater;

    private List<FilterJobType> filterJobTypes;
    private TextView spnSortType;
    //private ChipCloud relatedClassesChipCloud;
    //private HorizontalScrollView chipCloudRelativeClassesParent;

    private List<String> relativeSearchClasses = new ArrayList<>();
    private View selectedDateFilter;
    private SuggestionAutoCompleteView edtJobName;
    private AreaNameSuggestionCompleteView edtLocationName;
    private Button btnSearch;
    private TextView txtTotalCount;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_search_result, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        jobListAdapter = new JobListAdapter(context, jobList);
        lstSearchResult = (ListView) getActivity().findViewById(R.id.lst_search_result);

        jobListAdapter.setOnFavoriteClickListener(new JobListAdapter.OnFavoriteClickListener() {
            @Override
            public void onFavoriteClicked(Job job, int position) {

                boolean isFavorite = JobMgr.getInstance().isJobFavorited(jobListAdapter.getItem(position).getId());
                if (isFavorite) {
                    jobListAdapter.showFavoriteOptionsAtPosition(position);
                    lstSearchResult.getChildAt(position - lstSearchResult.getFirstVisiblePosition() + 1)
                            .findViewById(R.id.ll_favorite_jobs_menu).setVisibility(View.VISIBLE);
                }
            }
        });

        inflater = LayoutInflater.from(context);
        searchBoxHeader = inflater.inflate(R.layout.view_search_result_header, null);

        spnJobType = (TextView) searchBoxHeader.findViewById(R.id.spn_filter_job_type);

        spnSortType = (TextView) searchBoxHeader.findViewById(R.id.spn_sort_type);

        spnJobLocation = (TextView) searchBoxHeader.findViewById(R.id.spn_filter_location);
        searchBoxHeader.findViewById(R.id.img_clear_selected_filter_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                secondFilterLocation = null;
                selectedFilterLocation = primaryFilterLocation;
                createOrRefreshView();
            }
        });
        searchBoxHeader.findViewById(R.id.img_clear_selected_job_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedJobType = null;
                createOrRefreshView();
            }
        });

        locNames = new ArrayList<>();

        lstSearchResult.addHeaderView(searchBoxHeader);
        lstSearchResult.setAdapter(jobListAdapter);
        lstSearchResult.setOnItemClickListener(clickItemSearchResult);

        rowPrgCommentWaiting = LayoutInflater.from(context).inflate(R.layout.row_prg_waiting, null);
        rowRetryConnectionComment = LayoutInflater.from(context).inflate(R.layout.view_error_connection, null);

        searchText = getActivity().getIntent().getStringExtra("query");

        Integer lastLocationId = null;
        String lIds = getActivity().getIntent().getExtras().getString("locationId");
        if (lIds != null && !lIds.equals("")) {
            for (String s : lIds.split(",")) {
                if (!s.equals("") && !s.equals("null")) {
                    lastLocationId = Integer.valueOf(s);
                    break;
                }
            }
        }

        String locationName = getActivity().getIntent().getStringExtra("locationName");

        primaryFilterLocation = new FilterLocation(lastLocationId, locationName);
        selectedFilterLocation = primaryFilterLocation;

        saveSearchQuery();

        txtLocationName = (TextView) searchBoxHeader.findViewById(R.id.txt_location_name);
        if (selectedGeoLocation != null)
            txtLocationName.setText(locationName);

        prgDetectLocation = (ProgressBar) searchBoxHeader.findViewById(R.id.prg_detect_location);

        llCorrectQuery = (LinearLayout) searchBoxHeader.findViewById(R.id.ll_correct_query);
        txtCorrectQuery = (TextView) searchBoxHeader.findViewById(R.id.txt_correct_query);
        txtCorrectQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchMgr.getInstance().deleteSearchQueryByQuery(searchText);
                searchText = txtCorrectQuery.getText().toString().trim().replaceAll("\u00A0", "");

                saveSearchQuery();

                createOrRefreshView();
            }
        });

        selectedDateFilter = searchBoxHeader.findViewById(R.id.ll_selected_date);

        lastSearchDate = getActivity().getIntent().getLongExtra("lastSearchDate", 0l);

        lstSearchResult.setOnScrollListener(listScrollListener);

        edtJobName = (JobSuggestionCompleteView) searchBoxHeader.findViewById(R.id.edt_search_result_search_input);
        edtLocationName = (AreaNameSuggestionCompleteView) searchBoxHeader.findViewById(R.id.edt_search_result_search_location);
        btnSearch = (Button) searchBoxHeader.findViewById(R.id.btn_search_result_search);

        edtLocationName.setOnSuggestionChangedListener(new SuggestionAutoCompleteView.OnSuggestionChangedListener() {
            @Override
            public void onChangedText(String text) {
                primaryFilterLocation = new FilterLocation(null, text);
                selectedFilterLocation = primaryFilterLocation;
            }

            @Override
            public void onSelectedItem(Suggestion suggestion) {
                AreaNameSuggestion s = (AreaNameSuggestion) suggestion;
                primaryFilterLocation = new FilterLocation(s.getId(), s.getCity());
                selectedFilterLocation = primaryFilterLocation;
            }
        });

        edtJobName.disableTextWatcher();
        if (searchText != null) {
            edtJobName.setText(searchText);
            edtJobName.setSelection(edtJobName.getText().length());
            edtJobName.setClicked();
        }
        edtJobName.enableTextWatcher();

        edtLocationName.disableTextWatcher();
        if(locationName != null) {
            edtLocationName.setText(locationName);
            edtLocationName.setSelectedArea(new AreaNameSuggestion(lastLocationId, locationName));
        }
        else {
            edtLocationName.setText(getString(R.string.every_where));
        }
        edtLocationName.enableTextWatcher();

        edtJobName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if(actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }

                return false;
            }
        });

        edtLocationName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }

                return false;
            }
        });

        txtTotalCount = (TextView) searchBoxHeader.findViewById(R.id.txt_total_search_count);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSearch();
            }
        });

        createOrRefreshView();

        lstSearchResult.addFooterView(rowPrgCommentWaiting);
        lstSearchResult.addFooterView(rowRetryConnectionComment);
    }

    private void performSearch() {
        searchText = edtJobName.getText().toString();

        createOrRefreshView();

        View focusedView = getActivity().getCurrentFocus();
        if(focusedView != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }

        saveSearchQuery();
    }

    private void saveSearchQuery() {
        SearchMgr.getInstance().saveSearchQuery(new SearchHistory(searchText
                , selectedFilterLocation != null ? selectedFilterLocation.getId()+"" : null
                , selectedFilterLocation != null ? selectedFilterLocation.getLocationName() : null
                , System.currentTimeMillis()));
    }

    public void refreshList() {
        if (jobListAdapter != null)
            jobListAdapter.notifyDataSetChanged();
    }

    public void createOrRefreshView() {

        jobList.clear();
        lastLoadedItem = 0;
        totalLoadedItem = 0;
        completeLoadedPart = 0;

        jobListAdapter.notifyDataSetChanged();
    }

    AbsListView.OnScrollListener listScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            lastLoadedItem = firstVisibleItem + visibleItemCount;
            totalLoadedItem = totalItemCount;

            downloadResultPart();
        }
    };

    private void downloadResultPart() {
        if (lastLoadedItem == totalLoadedItem) {
            int currentPart = (int) (Math.ceil((totalLoadedItem - 2) / 20) - 1);
            int requestPart = currentPart + 1;
            if (requestPart >= completeLoadedPart) {
                addOrRefreshSearchResult(requestPart);
                rowPrgCommentWaiting.setVisibility(View.VISIBLE);
                rowRetryConnectionComment.setVisibility(View.INVISIBLE);
                completeLoadedPart++;
            }
        }
    }

    AdapterView.OnItemClickListener clickItemSearchResult = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if(i > 0) {
                ((OnJobSelectedListener) context).onJobSelected(searchText, (Job) adapterView.getItemAtPosition(i));
            }
        }
    };

    private void addOrRefreshSearchResult(final int requestPart) {

        Map<String, String> queries = new HashMap<>();

        if (searchText != null) {
            queries.put("q", searchText);
        }

        if (selectedJobType != null) {
            queries.put("jobType", selectedJobType.name());
        }

        if (selectedDistance != null && selectedGeoLocation != null) {
            queries.put("dis", selectedDistance + "");
            queries.put("lat", selectedGeoLocation.getLat() + "");
            queries.put("lon", selectedGeoLocation.getLon() + "");
        }

        if(selectedFilterLocation != null) {
            if(selectedFilterLocation.getId() != null) {
                queries.put("locationId", selectedFilterLocation.getId() + "");
            }
            else if(selectedFilterLocation.getLocationName() != null &&
                    !selectedFilterLocation.getLocationName().trim().equals("")) {
                queries.put("locationName", selectedFilterLocation.getLocationName());
            }
        }

        queries.put("page", requestPart + "");

        if (sortType != null) {
            queries.put("sortType", sortType);
        }

        if(lastSearchDate != null && lastSearchDate > 0) {
            queries.put("lastSearchDate", lastSearchDate+"");
        }

        ArekaWebService.getInstance().getWebService().searchJobs(queries).enqueue(new Callback<PageList<Job>>() {
            @Override
            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {

                PageList<Job> jobs = response.body();
                if (jobs == null || jobs.getItems() == null)
                    return;

                if (!isAdded())
                    return;

                rowPrgCommentWaiting.setVisibility(View.GONE);

                filterLocations = customizeFilterLocations(jobs.getFilterLocations());
                filterJobTypes = jobs.getFilterJobTypes();

                if(requestPart == 0 && jobs.getTotalCount() == 0 && lastSearchDate
                        > System.currentTimeMillis() - (24*60*60*1000)) {
                    lastSearchDate = 0l;
                    createOrRefreshView();
                    return;
                }

                if (requestPart == 0) {

                    txtTotalCount.setText(WordNormalizer.getInstance().toArabicDigits(jobs.getTotalCount()));

                    if (selectedJobType == null) {

                        DroppyMenuPopup.Builder jobTypeFilterBuilder = new DroppyMenuPopup.Builder(getContext()
                                , getView().findViewById(R.id.ll_filter_job_type));

                        for (FilterJobType fjt : filterJobTypes) {
                            jobTypeFilterBuilder.addMenuItem(new DroppyMenuItem(JobType.valueOf(fjt.getType()) + " ("
                                    + WordNormalizer.getInstance().toArabicDigits(fjt.getCount())
                                    + ")")).addSeparator();
                        }

                        jobTypeFilterBuilder.setOnClick(new DroppyClickCallbackInterface() {
                            @Override
                            public void call(View v, int index) {
                                selectedJobType = JobType.getByText(filterJobTypes.get(index).getType().toString());
                                createOrRefreshView();
                            }
                        });
                        jobTypeFilterBuilder.build();

                        getView().findViewById(R.id.ll_filter_job_type).setVisibility(View.VISIBLE);
                        getView().findViewById(R.id.ll_selected_job_type).setVisibility(View.GONE);
                    } else {
                        ((TextView) getView().findViewById(R.id.selected_filter_job_type))
                                .setText(selectedJobType.toString());

                        getView().findViewById(R.id.ll_filter_job_type).setVisibility(View.GONE);
                        getView().findViewById(R.id.ll_selected_job_type).setVisibility(View.VISIBLE);
                    }


                    if (secondFilterLocation == null) {
                        locNames.clear();

                        if(filterLocations != null && !filterLocations.isEmpty()) {
                            getView().findViewById(R.id.ll_filter_location_parent).setVisibility(View.VISIBLE);

                            DroppyMenuPopup.Builder jobLocationFilterBuilder = new DroppyMenuPopup.Builder(getContext()
                                    , getView().findViewById(R.id.ll_filter_location));

                            for (FilterLocation fl : filterLocations) {
                                jobLocationFilterBuilder.addMenuItem(new DroppyMenuItem(fl.getLocationName()
                                        + " ("
                                        + WordNormalizer.getInstance().toArabicDigits(fl.getCount())
                                        + ")"));
                            }
                            jobLocationFilterBuilder.setOnClick(new DroppyClickCallbackInterface() {
                                @Override
                                public void call(View v, int index) {
                                    FilterLocation loc = filterLocations.get(index);

                                    if (loc.getId() == null) {
                                        Toast.makeText(getActivity(), getString(R.string.item_cannot_be_selected)
                                                , Toast.LENGTH_LONG).show();
                                        return;
                                    }

                                    secondFilterLocation = filterLocations.get(index);
                                    selectedFilterLocation = secondFilterLocation;

                                    saveSearchQuery();

                                    createOrRefreshView();
                                }
                            });
                            jobLocationFilterBuilder.build();

                            getView().findViewById(R.id.ll_filter_location).setVisibility(View.VISIBLE);
                            getView().findViewById(R.id.ll_selected_location).setVisibility(View.GONE);
                        }
                        else {
                            getView().findViewById(R.id.ll_filter_location_parent).setVisibility(View.GONE);
                        }
                    } else {

                        ((TextView) getView().findViewById(R.id.selected_filter_location))
                                .setText(secondFilterLocation.getLocationName());

                        getView().findViewById(R.id.ll_filter_location).setVisibility(View.GONE);
                        getView().findViewById(R.id.ll_selected_location).setVisibility(View.VISIBLE);
                    }

                    DroppyMenuPopup.Builder sortTypeBuilder = new DroppyMenuPopup.Builder(getContext()
                            , getView().findViewById(R.id.ll_sort_type));

                    if (sortType == "relevance") {
                        sortTypeBuilder.addMenuItem(new DroppyMenuItem(getString(R.string.sort_by_relevancy)
                                , R.drawable.ic_action_check));

                        sortTypeBuilder.addMenuItem(new DroppyMenuItem(getString(R.string.sort_by_date)));

                        spnSortType.setText(getString(R.string.sort_by_relevancy_txt));
                        spnSortType.setTypeface(Typeface.DEFAULT);
                    }
                    else {
                        sortTypeBuilder.addMenuItem(new DroppyMenuItem(getString(R.string.sort_by_relevancy)));

                        sortTypeBuilder.addMenuItem(new DroppyMenuItem(getString(R.string.sort_by_date)
                                , R.drawable.ic_action_check));

                        spnSortType.setText(getString(R.string.sort_by_date_txt));
                        spnSortType.setTypeface(Typeface.DEFAULT_BOLD);
                    }

                    sortTypeBuilder.setOnClick(new DroppyClickCallbackInterface() {
                        @Override
                        public void call(View v, int index) {
                            if (index == 0) {
                                sortType = "relevance";
                            }
                            else {
                                sortType = "date";
                            }

                            createOrRefreshView();
                        }
                    });

                    sortTypeBuilder.build();

                    /*
                    List<SimpleEntry> relatedClasses = jobs.getRelatedClassBulks();
                    if(relatedClasses != null) {

                        chipCloudRelativeClassesParent.setVisibility(View.VISIBLE);
                        relatedClassesChipCloud.removeAllViews();

                        Collections.reverse(relatedClasses);

                        relativeSearchClasses.clear();
                        for(SimpleEntry c : relatedClasses) {
                            relativeSearchClasses.add(c.getKey());
                        }

                        if (relatedClasses != null) {
                            for (SimpleEntry c : relatedClasses) {
                                relatedClassesChipCloud.addChip(c.getKey() + " ("
                                        + WordNormalizer.getInstance().toArabicDigits(c.getCount()) + ") ");
                            }
                        }
                    }
                    else {
                        chipCloudRelativeClassesParent.setVisibility(View.GONE);
                    }
                    */

                    if(lastSearchDate > 0l) {
                        sortType = "date";
                        selectedDateFilter.setVisibility(View.VISIBLE);

                        ((TextView) getView().findViewById(R.id.txt_selected_date)).setText(
                                getString(R.string.from) + " " + DateUtil.getTimeAgo(lastSearchDate));

                        getView().findViewById(R.id.img_clear_selected_date).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                selectedDateFilter.setVisibility(View.GONE);
                                lastSearchDate = 0l;
                                createOrRefreshView();
                            }
                        });
                    }
                    else {
                        selectedDateFilter.setVisibility(View.GONE);
                    }
                }

                String correctQuery = jobs.getCorrectQuery();
                if (correctQuery != null) {
                    llCorrectQuery.setVisibility(View.VISIBLE);
                    txtCorrectQuery.setText('\u00A0' + correctQuery + '\u00A0');
                } else {
                    llCorrectQuery.setVisibility(View.GONE);
                }

                if (jobs.getItems().isEmpty() && jobList.isEmpty()) {
                    Toast.makeText(context, getString(R.string.not_found_any_result)
                            , Toast.LENGTH_SHORT).show();
                }

                //String totalCount = WordNormalizer.getInstance().toArabicDigits(jobs.getTotalCount());
                //searchResultCount.setText(getString(R.string.total_search_result) + " " + totalCount);

                jobList.addAll(jobs.getItems());
                jobListAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<PageList<Job>> call, Throwable t) {
                completeLoadedPart--;
                rowPrgCommentWaiting.setVisibility(View.INVISIBLE);
                rowRetryConnectionComment.setVisibility(View.VISIBLE);
                ImageView btnError = (ImageView) rowRetryConnectionComment.findViewById(R.id.btn_error);
                btnError.setColorFilter(Color.parseColor("#a9a9a9"));
                btnError.setOnClickListener(clickRetryGetComments);
            }
        });
    }

    View.OnClickListener clickRetryGetComments = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rowRetryConnectionComment.setVisibility(View.INVISIBLE);
            downloadResultPart();
        }
    };

    private List<FilterLocation> customizeFilterLocations(List<FilterLocation> filterLocations) {

        List<FilterLocation> filteredFilterLocations = new ArrayList<>();

        FilterLocation unknownFilterLocation = new FilterLocation();
        unknownFilterLocation.setLocationName(getString(R.string.unknowns));

        FilterLocation untitledFilterLocation = new FilterLocation();
        untitledFilterLocation.setLocationName(getString(R.string.untitleds));

        for (FilterLocation filterLocation : filterLocations) {
            String name = filterLocation.getLocationName();

            if (name.equals("unknown")) {
                unknownFilterLocation.setCount(unknownFilterLocation.getCount() + filterLocation.getCount());
            } else if (name.equals("untitled")) {
                untitledFilterLocation.setCount(untitledFilterLocation.getCount() + filterLocation.getCount());
            } else {
                filteredFilterLocations.add(filterLocation);
            }
        }

        Collections.sort(filteredFilterLocations, Collections.reverseOrder(new Comparator<FilterLocation>() {
            @Override
            public int compare(FilterLocation f1, FilterLocation f2) {
                return ((Integer) f1.getCount()).compareTo(f2.getCount());
            }
        }));

        if (filteredFilterLocations.size() > 0) {
            if (untitledFilterLocation.getCount() > 0) {
                filteredFilterLocations.add(untitledFilterLocation);
            }
            if (unknownFilterLocation.getCount() > 0) {
                filteredFilterLocations.add(unknownFilterLocation);
            }
        }

        return filteredFilterLocations;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (jobListAdapter != null)
            jobListAdapter.notifyDataSetChanged();
    }

}