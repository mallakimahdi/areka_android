package ir.areka.app.fragment;

import ir.areka.app.model.company.Company;

/**
 * Created by mahdi on 11/11/16.
 */

public interface OnCompanySelected {

    void onSelectCompany(Company company);

}
