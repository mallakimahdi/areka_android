package ir.areka.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.squareup.picasso.Picasso;

import ir.areka.app.R;
import ir.areka.app.model.company.Company;
import ir.areka.app.model.photo.Photo;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 11/11/16.
 */

public class CompanyPageFragment extends Fragment {

    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private Company company;
    private ProgressBar prgCompanyLoading;
    private Call<Company> getCompanyCall;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_company, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        prgCompanyLoading = (ProgressBar) getView().findViewById(R.id.prg_company_loading);

        mPager = (ViewPager) getView().findViewById(R.id.pager_company_details);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        PagerSlidingTabStrip indicator = (PagerSlidingTabStrip) getView().findViewById(R.id.view_pager_titles);
        indicator.setViewPager(mPager);
        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);

        createOrRefreshView();
    }

    private void createOrRefreshView() {

        if(company == null) {
            return;
        }

        if(company.getName() != null) {
            ((TextView) getView().findViewById(R.id.txt_company_name)).setText(company.getName());
        }

        getCompanyCall = ArekaWebService.getInstance().getWebService().getCompanyById(company.getId());
        getCompanyCall.enqueue(new Callback<Company>() {
            @Override
            public void onResponse(Call<Company> call, Response<Company> response) {
                company = response.body();

                if(company == null)
                    return;

                fragCompanyAbout.setCompany(company);
                fragCompanyComment.setCompany(company);
                fragCompanyJobs.setCompany(company);

                if(company.getPhoto() != null) {
                    for(Photo photo : company.getPhoto()) {
                        if(photo.getTag().contains("logo")) {
                            ImageView imageView = (ImageView) getView().findViewById(R.id.img_company_logo);
                            imageView.setVisibility(View.VISIBLE);
                            Picasso.with(getActivity()).load(photo.getUrl()).into(imageView);
                        }
                    }
                }

                prgCompanyLoading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Company> call, Throwable t) {
                prgCompanyLoading.setVisibility(View.GONE);
            }
        });
    }

    CompanyAboutPageFragment fragCompanyAbout = new CompanyAboutPageFragment();
    CompanyCommentPageFragment fragCompanyComment = new CompanyCommentPageFragment();
    CompanyJobsPageFragment fragCompanyJobs = new CompanyJobsPageFragment();

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    fragCompanyJobs.setCompany(company);
                    return fragCompanyJobs;
                case 1:
                    fragCompanyComment.setCompany(company);
                    return fragCompanyComment;
                case 2:
                    fragCompanyAbout.setCompany(company);
                    return fragCompanyAbout;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.jobs);
                case 1:
                    return getString(R.string.comments);
                case 2:
                    return getString(R.string.about);
            }

            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getCompanyCall != null) {
            getCompanyCall.cancel();
        }
    }

    public Fragment setCompany(Company company) {
        this.company = company;
        return this;
    }
}
