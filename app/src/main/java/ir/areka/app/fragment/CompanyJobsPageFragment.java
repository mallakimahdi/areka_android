package ir.areka.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.adapter.JobListAdapter;
import ir.areka.app.model.company.Company;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.PageList;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 11/11/16.
 */

public class CompanyJobsPageFragment extends Fragment {

    Company company;
    List<Job> jobList = new ArrayList<>();
    private Context context;
    private JobListAdapter jobListAdapter;
    private boolean isSeenFirst = true;
    private ProgressBar prgLoadingCompanyJobs;
    private Call<PageList<Job>> getCompanyJobsCall;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_company_jobs, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        jobListAdapter = new JobListAdapter(context, jobList);
        ListView lstSearchResult = (ListView) getActivity().findViewById(R.id.lst_company_jobs);
        lstSearchResult.setAdapter(jobListAdapter);
        prgLoadingCompanyJobs = (ProgressBar) getView().findViewById(R.id.prg_loading_company_jobs);
    }

    private void createOrRefreshView() {
        if(!isSeenFirst)
            return;

        prgLoadingCompanyJobs.setVisibility(View.VISIBLE);
        getCompanyJobsCall = ArekaWebService.getInstance().getWebService().getCompanyJobs(company.getId());
        getCompanyJobsCall.enqueue(new Callback<PageList<Job>>() {
            @Override
            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {
                PageList jobs = response.body();

                if (jobs == null || jobs.getItems() == null)
                    return;

                jobList.clear();
                jobList.addAll(jobs.getItems());
                jobListAdapter.notifyDataSetChanged();

                prgLoadingCompanyJobs.setVisibility(View.GONE);
                isSeenFirst = false;
            }

            @Override
            public void onFailure(Call<PageList<Job>> call, Throwable t) {
                prgLoadingCompanyJobs.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        createOrRefreshView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getCompanyJobsCall != null) {
            getCompanyJobsCall.cancel();
        }
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
