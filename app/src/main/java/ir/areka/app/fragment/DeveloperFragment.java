package ir.areka.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import ir.areka.app.R;

/**
 * Created by mahdi on 7/29/16.
 */
public class DeveloperFragment extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        this.context = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frg_developers, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createOrRefreshView();
    }

    public void createOrRefreshView() {
        try {
            InputStream stream = getResources().openRawResource(R.raw.developer_text);

            ((TextView) getView().findViewById(R.id.txt_developer_text)).setText(readStream(stream));

            stream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) getView().findViewById(R.id.txt_developer_text)).setMovementMethod(LinkMovementMethod.getInstance());

    }

    public static String readStream(InputStream is) {
        StringBuilder sb = new StringBuilder(512);
        try {
            Reader r = new InputStreamReader(is, "UTF-8");
            int c = 0;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

}
