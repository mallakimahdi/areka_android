package ir.areka.app.model.suggestion;

/**
 * Created by mahdi on 11/18/16.
 */

public class JobSuggestion implements Suggestion {

    private String text;

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        if(text != null)
            return text;
        return super.toString();
    }

}
