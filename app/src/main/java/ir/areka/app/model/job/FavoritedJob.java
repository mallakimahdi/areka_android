package ir.areka.app.model.job;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by mahdi on 11/28/16.
 */

public class FavoritedJob {

    @DatabaseField(id = true)
    private String jobId;

    @DatabaseField
    private Long date;

    public FavoritedJob() {}

    public FavoritedJob(String jobId, Long date) {
        this.jobId = jobId;
        this.date = date;
    }

    public FavoritedJob(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
