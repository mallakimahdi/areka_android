package ir.areka.app.model.photo;

import java.util.List;

/**
 * Created by mahdi on 5/31/17.
 */

public class Photo {

    private String id;
    private List<String> tag;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTag() {
        return tag;
    }

    public void setTag(List<String> tag) {
        this.tag = tag;
    }
}
