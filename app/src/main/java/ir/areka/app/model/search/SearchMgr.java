package ir.areka.app.model.search;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ir.areka.app.activity.App;
import ir.areka.app.model.base.DatabaseHelper;

/**
 * Created by mahdi on 7/21/16.
 */
public class SearchMgr {

    static SearchMgr instance;
    private final RuntimeExceptionDao<SearchHistory, ?> searchHistoryDao;

    public synchronized static SearchMgr getInstance() {
        if(instance == null)
            instance = new SearchMgr();
        return instance;
    }

    private SearchMgr() {
        searchHistoryDao = DatabaseHelper.getInstance(App.getContext())
                .getRuntimeExceptionDao(SearchHistory.class);
    }

    public void saveSearchQuery(SearchHistory searchHistory) {
        searchHistoryDao.createOrUpdate(searchHistory);
    }

    public void deleteSearchQuery(SearchHistory searchHistory) {
        searchHistoryDao.delete(searchHistory);
    }

    public void deleteSearchQueryByQuery(String query) {
        DeleteBuilder<SearchHistory, ?> builder = searchHistoryDao.deleteBuilder();
        try {
            builder.where().eq("query", query);
            builder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            searchHistoryDao.deleteBuilder().delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SearchHistory> getSearchHistories() {
        try {
            return searchHistoryDao.queryBuilder().orderBy("lastUpdateDate", false).query();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }
}
