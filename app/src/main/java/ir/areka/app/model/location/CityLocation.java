package ir.areka.app.model.location;

/**
 * Created by mahdi on 11/18/16.
 */

public class CityLocation {

    private int id;
    private String city;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
