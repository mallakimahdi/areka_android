package ir.areka.app.model.comment;

/**
 * Created by mahdi on 11/13/16.
 */

public class CommentDTO {

    private String id;
    private String company;
    private String username;
    private String comment;
    private Integer stars;

    public CommentDTO() {
    }

    public void loadFrom(Comment comment) {
        this.id = comment.getId();
        this.company = comment.getCompany().getId();
        this.username = comment.getUsername();
        this.comment = comment.getComment();
        this.stars = comment.getStars();
    }

    public void saveTo(Comment comment) {
        comment.setId(getId());
        comment.setUsername(getUsername());
        comment.setComment(getComment());
        comment.setStars(getStars());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }
}
