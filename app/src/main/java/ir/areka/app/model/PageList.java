package ir.areka.app.model;

import java.util.List;
import ir.areka.app.model.job.FilterJobType;
import ir.areka.app.model.job.SimpleEntry;
import ir.areka.app.model.location.FilterLocation;

/**
 * Created by mahdi on 3/22/16.
 */
public class PageList<T> {
    private int totalCount;
    private List<T> items;
    private String correctQuery;
    private List<FilterLocation> filterLocations;
    private List<FilterJobType> filterJobTypes;
    private List<SimpleEntry> relatedClassBulks;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public String getCorrectQuery() {
        return correctQuery;
    }

    public void setCorrectQuery(String correctQuery) {
        this.correctQuery = correctQuery;
    }

    public List<FilterLocation> getFilterLocations() {
        return filterLocations;
    }

    public void setFilterLocations(List<FilterLocation> filterLocations) {
        this.filterLocations = filterLocations;
    }

    public List<FilterJobType> getFilterJobTypes() {
        return filterJobTypes;
    }

    public void setFilterJobTypes(List<FilterJobType> filterJobTypes) {
        this.filterJobTypes = filterJobTypes;
    }

    public List<SimpleEntry> getRelatedClassBulks() {
        return relatedClassBulks;
    }

    public void setRelatedClassBulks(List<SimpleEntry> relatedClassBulks) {
        this.relatedClassBulks = relatedClassBulks;
    }
}
