package ir.areka.app.model.job;

import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ir.areka.app.activity.App;
import ir.areka.app.model.base.DatabaseHelper;

/**
 * Created by mahdi on 3/22/16.
 */
public class JobMgr {

    private final RuntimeExceptionDao<FavoritedJob, ?> favoritedDao;
    private final RuntimeExceptionDao<ViewedJob, ?> viewedJobDao;
    private static JobMgr jobMgr;
    SharedPreferences preferences = App.getContext().getSharedPreferences("areka", Context.MODE_PRIVATE);

    public static JobMgr getInstance() {
        if (jobMgr == null)
            jobMgr = new JobMgr();
        return jobMgr;
    }

    private JobMgr() {
        favoritedDao = DatabaseHelper.getInstance(App.getContext())
                .getRuntimeExceptionDao(FavoritedJob.class);
        viewedJobDao = DatabaseHelper.getInstance(App.getContext())
                .getRuntimeExceptionDao(ViewedJob.class);
    }

    public void addJobToViewed(String jobId) {

        if(viewedJobDao.countOf() > 100) {

            try {
                viewedJobDao.delete(viewedJobDao.queryBuilder().orderBy("date", true).query().subList(0, 10));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        viewedJobDao.createOrUpdate(new ViewedJob(jobId, System.currentTimeMillis()));
    }

    public Long getJobVisitedDate(String jobId) {
        ViewedJob item = viewedJobDao.queryForSameId(new ViewedJob(jobId));
        if(item != null)
            return item.getDate();

        return null;
    }

    public List<ViewedJob> getVisitedJobs() {
        try {
            return viewedJobDao.queryBuilder().orderBy("date", false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public void addJobToFavorited(String jobId) {

        if(favoritedDao.countOf() > 50) {
            try {
                favoritedDao.delete(favoritedDao.queryBuilder().orderBy("date", true).query().subList(0, 10));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        favoritedDao.createOrUpdate(new FavoritedJob(jobId, System.currentTimeMillis()));
    }

    public boolean isJobFavorited(String jobId) {
        return favoritedDao.queryForSameId(new FavoritedJob(jobId)) != null;
    }

    public List<FavoritedJob> getAllFavoritedJobs() {
        try {
            return favoritedDao.queryBuilder().orderBy("date", false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public void removeFromFavorite(String jobId) {
        favoritedDao.delete(new FavoritedJob(jobId));
    }

    public long getLastVisitedRecommended() {
        return preferences.getLong("lastVisitedRecommended", 0l);
    }

    public void setLastVisitedRecommendedJobs(long date) {
        preferences.edit().putLong("lastVisitedRecommended", date).apply();
    }
}
