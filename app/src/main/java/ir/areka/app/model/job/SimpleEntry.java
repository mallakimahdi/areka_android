package ir.areka.app.model.job;

/**
 * Created by mahdi on 4/24/17.
 */

public class SimpleEntry {
    private String key;
    private Integer count;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String toString() {
        return this.key + "=" + this.count;
    }
}
