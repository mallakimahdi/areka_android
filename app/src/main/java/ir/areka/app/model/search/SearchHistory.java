package ir.areka.app.model.search;

import com.j256.ormlite.field.DatabaseField;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahdi on 7/23/16.
 */
public class SearchHistory {

    @DatabaseField(id = true, useGetSet = true)
    private String id;

    @DatabaseField
    private String query;

    @DatabaseField
    private String locationIds;

    @DatabaseField
    private String locationName;

    @DatabaseField
    private long lastUpdateDate;

    private int newJobs;

    public SearchHistory() {
    }

    public SearchHistory(String query, String locationIds, String locationName, long updateDate) {
        this.query = query;
        this.locationIds = locationIds;
        this.locationName = locationName;
        this.lastUpdateDate = updateDate;
    }

    public String getId() {
        if(locationIds != null) {
            return query + "," + StringUtils.join(locationIds, ",");
        }
        return query;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNewJobs() {
        return newJobs;
    }

    public void setNewJobs(int newJobs) {
        this.newJobs = newJobs;
    }

    public long getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(long lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationIds() {
        return locationIds;
    }

    public void setLocationIds(String locationIds) {
        this.locationIds = locationIds;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Integer> getLocationIdsArray() {
        List<Integer> ids = new ArrayList<>();

        if(getLocationIds() != null) {
            String[] lIds = getLocationIds().split(",");
            for (String lId : lIds) {
                if(!lId.equals("") && !lId.equals("null")) {
                    ids.add(Integer.valueOf(lId));
                }
            }
        }
        return ids;
    }

}
