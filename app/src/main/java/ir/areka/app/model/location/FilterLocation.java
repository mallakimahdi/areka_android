package ir.areka.app.model.location;

/**
 * Created by mahdi on 2/13/17.
 */

public class FilterLocation {

    private Integer id;
    private String locationName;
    private int count;

    public FilterLocation() { }

    public FilterLocation(Integer id, String locationName) {
        this.id = id;
        this.locationName = locationName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
