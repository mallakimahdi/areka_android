package ir.areka.app.model;

import java.util.List;

/**
 * Created by mahdi on 12/6/16.
 */

public class UpdateDTO {

    private long startTime;
    private long timeOut;
    private List<String> addressList;
    private int retryCount;
    private String retryTimeUnit;

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public List<String> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<String> addressList) {
        this.addressList = addressList;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public String getRetryTimeUnit() {
        return retryTimeUnit;
    }

    public void setRetryTimeUnit(String retryTimeUnit) {
        this.retryTimeUnit = retryTimeUnit;
    }
}
