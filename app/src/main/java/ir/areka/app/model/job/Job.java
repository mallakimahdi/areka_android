package ir.areka.app.model.job;

import java.util.List;
import java.util.Set;

import ir.areka.app.model.company.Company;
import ir.areka.app.model.location.GeoLocation;

/**
 * Created by mahdi on 3/22/16.
 */
public class Job {
    String id;

    String title;
    String jobName;
    String address;
    List<String> body;

    List<String> email;
    List<String> pictures;
    List<String> phoneNumber;

    List<LocationName> locationNames;

    String sourceCrawle;

    Company company;
    long date;

    private Set<JobType> jobType;
    private List<GeoLocation> geoLocations;

    private boolean isFavorite;
    private boolean openInBrowser;

    private List<String> highlight;

    public Set<JobType> getJobType() {
        return jobType;
    }

    public void setJobType(Set<JobType> jobType) {
        this.jobType = jobType;
    }

    public List<GeoLocation> getGeoLocations() {
        return geoLocations;
    }

    public void setGeoLocations(List<GeoLocation> geoLocations) {
        this.geoLocations = geoLocations;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(List<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public long getDate() {
        return date;
    }

    public List<LocationName> getLocationNames() {
        return locationNames;
    }

    public void setLocationNames(List<LocationName> locationNames) {
        this.locationNames = locationNames;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public List<String> getBody() {
        return body;
    }

    public void setBody(List<String> body) {
        this.body = body;
    }

    public String getSourceCrawle() {
        return sourceCrawle;
    }

    public void setSourceCrawle(String sourceCrawle) {
        this.sourceCrawle = sourceCrawle;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isOpenInBrowser() {
        return openInBrowser;
    }

    public void setOpenInBrowser(boolean openInBrowser) {
        this.openInBrowser = openInBrowser;
    }

    public List<String> getHighlight() {
        return highlight;
    }

    public void setHighlight(List<String> highlight) {
        this.highlight = highlight;
    }

    public class LocationName {
        private String cityName;
        private String stateName;

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }
    }
}
