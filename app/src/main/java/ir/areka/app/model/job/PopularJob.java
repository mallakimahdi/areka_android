package ir.areka.app.model.job;

/**
 * Created by mahdi on 9/18/16.
 */
public class PopularJob {
    String key;
    int doc_count;

    public PopularJob(String key, int doc_count) {
        this.key = key;
        this.doc_count = doc_count;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getDoc_count() {
        return doc_count;
    }

    public void setDoc_count(int doc_count) {
        this.doc_count = doc_count;
    }
}
