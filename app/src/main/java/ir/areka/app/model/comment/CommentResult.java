package ir.areka.app.model.comment;

/**
 * Created by mahdi on 4/23/17.
 */

public class CommentResult {

    private String _id;
    private String result;
    private boolean created;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }
}
