package ir.areka.app.model.location;

/**
 * Created by mahdi on 4/8/17.
 */

public class UserCity {
    private int id;
    private String city;
    private long date;

    private double lat;
    private double lon;

    public UserCity(int id, String city, long date) {
        this.id = id;
        this.city = city;
        this.date = date;
    }

    public UserCity(int id, String city, long date, double lat, double lon) {
        this.id = id;
        this.city = city;
        this.date = date;
        this.lat = lat;
        this.lon = lon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
