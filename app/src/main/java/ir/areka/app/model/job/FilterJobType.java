package ir.areka.app.model.job;

/**
 * Created by mahdi on 2/25/17.
 */

public class FilterJobType {
    private String type;
    private int count;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
