package ir.areka.app.model.job;

/**
 * Created by mahdi on 11/18/16.
 */

public class NewJobsDTO {

    int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
