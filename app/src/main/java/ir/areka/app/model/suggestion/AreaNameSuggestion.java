package ir.areka.app.model.suggestion;

import ir.areka.app.R;
import ir.areka.app.activity.App;

/**
 * Created by mahdi on 11/18/16.
 */

public class AreaNameSuggestion implements Suggestion {

    private Integer id;
    private String city;
    private String state;

    public AreaNameSuggestion(Integer id, String city) {
        this.id = id;
        this.city = city;
    }

    public AreaNameSuggestion() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        String str = "";
        if (city != null) {
            str+= city;
        }
        if (state != null) {
            str += App.getContext().getString(R.string.comma) +" "+ state;
        }
        return str;
    }
}
