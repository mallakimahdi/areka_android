package ir.areka.app.model.job;

import ir.areka.app.R;
import ir.areka.app.activity.App;

public enum JobType {
	FULL_TIME(App.getContext().getString(R.string.full_time)),
	PART_TIME(App.getContext().getString(R.string.part_time)),
	INTERNSHIP(App.getContext().getString(R.string.internship));

	private final String text;

	/**
	 * @param text
	 */
	JobType(final String text) {
		this.text = text;
	}

	/* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
	@Override
	public String toString() {
		return text;
	}

	public static JobType getByText(String txt) {
		JobType jt = null;

		if(txt.equals("FULL_TIME")) {
			jt = JobType.FULL_TIME;
		}
		else if(txt.equals("PART_TIME")) {
			jt = JobType.PART_TIME;
		}
		else if(txt.equals("INTERNSHIP")) {
			jt = JobType.INTERNSHIP;
		}

		return jt;
	}

	public static JobType getByValueText(String txt) {
		JobType jt = null;

		if(txt.equals(App.getContext().getString(R.string.full_time))) {
			jt = JobType.FULL_TIME;
		}
		else if(txt.equals(App.getContext().getString(R.string.part_time))) {
			jt = JobType.PART_TIME;
		}
		else if(txt.equals(App.getContext().getString(R.string.internship))) {
			jt = JobType.INTERNSHIP;
		}

		return jt;
	}
}
