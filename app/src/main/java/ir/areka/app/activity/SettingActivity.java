package ir.areka.app.activity;

import android.os.Bundle;

import ir.areka.app.R;
import ir.areka.app.fragment.SettingsFragment;

/**
 * Created by mahdi on 11/3/16.
 */

public class SettingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, new SettingsFragment(), "settings")
                    .commit();
        }

    }

    @Override
    protected String getTag() {
        return ActivityTags.SETTINGS.getTag();
    }
}
