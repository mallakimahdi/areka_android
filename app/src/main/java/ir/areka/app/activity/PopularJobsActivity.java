package ir.areka.app.activity;

import android.os.Bundle;

import ir.areka.app.R;
import ir.areka.app.fragment.PopularJobsFragment;

/**
 * Created by mahdi on 9/17/16.
 */
public class PopularJobsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, new PopularJobsFragment(), "developer")
                    .commit();
    }

    @Override
    protected String getTag() {
        return ActivityTags.POPULAR_JOBS.getTag();
    }
}
