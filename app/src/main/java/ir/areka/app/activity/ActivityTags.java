package ir.areka.app.activity;

public enum ActivityTags
{
    HOME("home"), DEVELOPERS("developers"), POPULAR_JOBS("popularJobs"), FAVORITE("favorite"), SEARCH_RESULT("searchResult")
    , SETTINGS("settings"), VISITED_JOBS("visitedJobs"), RECOMMENDED_JOBS("recommended");

    private String tag;

    ActivityTags(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
