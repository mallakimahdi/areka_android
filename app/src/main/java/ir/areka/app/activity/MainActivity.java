package ir.areka.app.activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.widget.Toast;

import ir.areka.app.R;
import ir.areka.app.dialog.ShowJobDialog;
import ir.areka.app.fragment.HomeFragment;
import ir.areka.app.util.ShakeDetector;

public class MainActivity extends BaseActivity {

    ActivityCompat.OnRequestPermissionsResultCallback requestPermissionsResultCallback;
    HomeFragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {

            homeFragment = new HomeFragment();
            homeFragment.setContextActivity(this);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, homeFragment
                            , "home")
                    .commit();

            requestPermissionsResultCallback = homeFragment;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected String getTag() {
        return ActivityTags.HOME.getTag();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        requestPermissionsResultCallback.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        homeFragment.onActivityResult(requestCode, resultCode, data);
    }
}
