package ir.areka.app.activity;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.UUID;

import ir.areka.app.net.ArekaWebService;
import ir.areka.app.util.NotificationCheckerService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 5/24/16.
 */
public class App extends MultiDexApplication {

    private static Context mContext;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        mContext.startService(new Intent(mContext, NotificationCheckerService.class));
    }

    public static String getDeviceUuid() {
        SharedPreferences preferences = getContext().getSharedPreferences("areka", Context.MODE_PRIVATE);
        String deviceUuid = preferences.getString("deviceUuid", "");
        if(deviceUuid.isEmpty()) {
            deviceUuid = UUID.randomUUID().toString();
            preferences.edit().putString("deviceUuid", deviceUuid).apply();
        }

        return deviceUuid;
    }

    public static void sendFirebaseTokenIsNotSent() {
        String token = FirebaseInstanceId.getInstance().getToken();
        ArekaWebService.getInstance().getWebService().sendFirebaseToken(token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) { }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) { }
        });
    }

    public static Context getContext() {
        return mContext;
    }

    public static String getVersionName() {
        try {
            return App.getContext().getPackageManager().getPackageInfo(App.getContext().getPackageName(), 0).versionName;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
