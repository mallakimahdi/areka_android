package ir.areka.app.activity;

import android.os.Bundle;

import ir.areka.app.R;
import ir.areka.app.fragment.FavoriteJobsFragment;
import ir.areka.app.fragment.OnJobSelectedListener;

/**
 * Created by mahdi on 9/23/16.
 */
public class FavoriteJobsActivity extends BaseActivity implements OnJobSelectedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, new FavoriteJobsFragment(), "favorite")
                    .commit();

    }

    @Override
    protected String getTag() {
        return ActivityTags.FAVORITE.getTag();
    }
}
