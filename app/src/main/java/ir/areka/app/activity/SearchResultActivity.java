package ir.areka.app.activity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import ir.areka.app.R;
import ir.areka.app.dialog.ShowJobDialog;
import ir.areka.app.fragment.SearchResultFragment;
import ir.areka.app.util.ShakeDetector;

/**
 * Created by mahdi on 3/20/16.
 */
public class SearchResultActivity extends BaseActivity {

    SearchResultFragment searchResultFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            searchResultFragment = new SearchResultFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, searchResultFragment, "searchResult")
                    .commit();

            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    if(searchResultFragment != null) {
                        searchResultFragment.refreshList();
                    }
                }
            });
        }

        getSupportActionBar().hide();
        findViewById(R.id.ll_top_menu).setVisibility(View.GONE);
    }

    @Override
    protected String getTag() {
        return ActivityTags.SEARCH_RESULT.getTag();
    }

}
