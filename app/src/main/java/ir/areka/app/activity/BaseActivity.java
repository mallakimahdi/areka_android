package ir.areka.app.activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import ir.areka.app.R;
import ir.areka.app.dialog.ShowJobDialog;
import ir.areka.app.fragment.CompanyPageFragment;
import ir.areka.app.fragment.JobPreviewFragment;
import ir.areka.app.fragment.OnCompanySelected;
import ir.areka.app.fragment.OnJobSelectedListener;
import ir.areka.app.model.company.Company;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.JobMgr;
import ir.areka.app.model.search.SearchMgr;
import ir.areka.app.util.ShakeDetector;

/**
 * Created by mahdi on 7/29/16.
 */
public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, OnJobSelectedListener
            , OnCompanySelected {

    private LayoutInflater inflater;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private View actionbar;

    private ShakeDetector mShakeDetector;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private Job currentJob;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        inflater = LayoutInflater.from(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slider_menu);
        actionbar = findViewById(R.id.ll_top_menu);

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                findViewById(R.id.main_view).setX(mDrawerList.getWidth() * slideOffset * -1);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        findViewById(R.id.btn_toggle_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.END))
                    closeMenuDrawable();
                else
                    openMenuDrawable();
            }
        });

        showHomeMenu();
        showFavoriteJobsMenu();
        showVisitedJobsMenu();

        showPopularJobsMenu();
        if(SearchMgr.getInstance().getSearchHistories().size() >= 2) {
            showRecommendedJobs();
        }
        showDeveloperMenu();
        //showSettingsMenu();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.row_right_menu);
        mDrawerList.setAdapter(adapter);

        getSupportActionBar().hide();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {

                if(count > 1) {
                    if(currentJob == null) {
                        ShowJobDialog dialog = new ShowJobDialog();
                        dialog.setActivity(BaseActivity.this);
                        dialog.show(getSupportFragmentManager(), null);
                    }
                    else {
                        Toast.makeText(BaseActivity.this, "id: "+ currentJob.getId(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    protected void showHomeMenu() {
        createNewRow(getString(R.string.home), R.drawable.ic_action_home, ActivityTags.HOME.getTag());
    }

    protected void showDeveloperMenu() {
        createNewRow(getString(R.string.developers), R.drawable.ic_action_developer_mode, ActivityTags.DEVELOPERS.getTag());
    }

    protected void showPopularJobsMenu() {
        createNewRow(getString(R.string.popular_jobs), R.drawable.ic_action_trending_up, ActivityTags.POPULAR_JOBS.getTag());
    }

    protected void showFavoriteJobsMenu() {
        createNewRow(getString(R.string.favorite_list), R.drawable.ic_action_favorite_dark, ActivityTags.FAVORITE.getTag());
    }
/*
    protected void showSettingsMenu() {
        createNewRow(getString(R.string.settings), R.drawable.settings, ActivityTags.SETTINGS.getTag());
    }
*/
    protected void showVisitedJobsMenu() {
        createNewRow(getString(R.string.visited_jobs), R.drawable.ic_action_history, ActivityTags.VISITED_JOBS.getTag());
    }

    protected void showRecommendedJobs() {
        createNewRow(getString(R.string.recommended_jobs), R.drawable.ic_action_bookmark_border, ActivityTags.RECOMMENDED_JOBS.getTag());
    }

    private ViewGroup createNewRow(String title, int imageResource, String tag) {
        ViewGroup ll = (ViewGroup) inflater.inflate(R.layout.row_right_menu, null);

        ((TextView) ll.findViewById(R.id.txt_right_menu)).setText(title);
        ((ImageView) ll.findViewById(R.id.img_right_menu)).setImageResource(imageResource);
        ll.setOnClickListener(this);
        ll.setTag(tag);
        mDrawerList.addHeaderView(ll);

        return ll;
    }

    @Override
    public void onClick(View view) {
        if(!getTag().equals(view.getTag())) {

            if (view.getTag().equals(ActivityTags.DEVELOPERS.getTag()))
                startActivity(new Intent(this, DeveloperActivity.class));
            else if (view.getTag().equals(ActivityTags.HOME.getTag())) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            else if (view.getTag().equals(ActivityTags.POPULAR_JOBS.getTag()))
                startActivity(new Intent(this, PopularJobsActivity.class));
            else if (view.getTag().equals(ActivityTags.FAVORITE.getTag()))
                startActivity(new Intent(this, FavoriteJobsActivity.class));
            else if (view.getTag().equals(ActivityTags.SETTINGS.getTag()))
                startActivity(new Intent(this, SettingActivity.class));
            else if(view.getTag().equals(ActivityTags.VISITED_JOBS.getTag()))
                startActivity(new Intent(this, VisitedJobsActivity.class));
            else if(view.getTag().equals(ActivityTags.RECOMMENDED_JOBS.getTag()))
                startActivity(new Intent(this, RecommendedJobsActivity.class));
        }

        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onJobSelected(String searchQuery, Job job) {

        if(job != null) {
            currentJob = job;
            JobMgr.getInstance().addJobToViewed(job.getId());

            if(job.isOpenInBrowser()) {
                Intent browserIntent = new Intent(this, WebViewActivity.class);
                browserIntent.putExtra("url", job.getSourceCrawle());
                startActivity(browserIntent);
            }
            else {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.view_main_frame
                        , new JobPreviewFragment().setJob(job).setSearchQuery(searchQuery)
                        , "jobPreview");
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        }
        else {
            Toast.makeText(this, getString(R.string.cannot_find_job), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSelectCompany(Company company) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.view_main_frame
                , new CompanyPageFragment() .setCompany(company), "companyPreview");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void showActionbar() {
        actionbar.setVisibility(View.VISIBLE);
    }

    protected abstract String getTag();

    private void closeMenuDrawable() {
        mDrawerLayout.closeDrawer(GravityCompat.END);
    }

    private void openMenuDrawable() {
        try {
            mDrawerLayout.openDrawer(GravityCompat.END);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }
}
