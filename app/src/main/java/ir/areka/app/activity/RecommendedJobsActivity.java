package ir.areka.app.activity;

import android.os.Bundle;

import ir.areka.app.R;
import ir.areka.app.fragment.RecommendedFragment;

/**
 * Created by mahdi on 11/28/16.
 */

public class RecommendedJobsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, new RecommendedFragment(), "recommended")
                    .commit();
    }

    @Override
    protected String getTag() {
        return ActivityTags.RECOMMENDED_JOBS.getTag();
    }

}
