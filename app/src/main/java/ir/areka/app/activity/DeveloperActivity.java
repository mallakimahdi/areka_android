package ir.areka.app.activity;

import android.os.Bundle;

import ir.areka.app.fragment.DeveloperFragment;
import ir.areka.app.R;

/**
 * Created by mahdi on 7/29/16.
 */
public class DeveloperActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, new DeveloperFragment(), "developer")
                    .commit();

    }

    @Override
    protected String getTag() {
        return ActivityTags.DEVELOPERS.getTag();
    }
}
