package ir.areka.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import ir.areka.app.R;

/**
 * Created by mahdi on 5/6/17.
 */

public class WebViewActivity extends Activity {

    private TextView urlPreview;
    private TextView headerPreview;
    private View imgOpenLinkInBrowser;
    private View prgLoadingPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_view);

        urlPreview = (TextView) findViewById(R.id.txt_url_preview);
        headerPreview = (TextView) findViewById(R.id.txt_header_preview);
        imgOpenLinkInBrowser = findViewById(R.id.img_open_in_browser);
        prgLoadingPage = findViewById(R.id.prg_loading_url);

        String url = getIntent().getStringExtra("url");

        WebView webView = (WebView) findViewById(R.id.link_preview_web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setInitialScale(100);
        webView.loadUrl(url);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, final String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                urlPreview.setText(url);

                prgLoadingPage.setVisibility(View.VISIBLE);
                headerPreview.setVisibility(View.GONE);

                imgOpenLinkInBrowser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                headerPreview.setText(view.getTitle());

                prgLoadingPage.setVisibility(View.GONE);
                headerPreview.setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.img_back_page).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
