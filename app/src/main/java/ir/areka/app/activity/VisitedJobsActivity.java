package ir.areka.app.activity;

import android.os.Bundle;

import ir.areka.app.R;
import ir.areka.app.fragment.OnJobSelectedListener;
import ir.areka.app.fragment.VisitedJobsFragment;

/**
 * Created by mahdi on 11/16/16.
 */

public class VisitedJobsActivity extends BaseActivity implements OnJobSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.view_main_frame, new VisitedJobsFragment(), "visited")
                    .commit();
    }

    @Override
    protected String getTag() {
        return ActivityTags.VISITED_JOBS.getTag();
    }

}
