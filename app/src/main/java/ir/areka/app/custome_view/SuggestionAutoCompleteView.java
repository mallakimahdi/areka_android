package ir.areka.app.custome_view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import java.util.List;

import ir.areka.app.adapter.SuggestListAdapter;
import ir.areka.app.model.suggestion.Suggestion;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 6/27/16.
 */
public abstract class SuggestionAutoCompleteView<T extends Suggestion> extends AppCompatAutoCompleteTextView {
    private SuggestListAdapter suggestionAdapter;
    private boolean clicked;
    protected Suggestion clickedItem;
    private Context mContext;
    private OnSuggestionChangedListener suggestionListener;

    public SuggestionAutoCompleteView(Context context) {
        super(context);
        init(context);
    }

    public SuggestionAutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(final Context context) {
        mContext = context;

        suggestionAdapter = new SuggestListAdapter(context);
        suggestionAdapter.setNotifyOnChange(true);
        setAdapter(suggestionAdapter);
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                clicked = true;
                clickedItem = suggestionAdapter.getItem(i);

                if(suggestionListener != null) {
                    suggestionListener.onSelectedItem(clickedItem);
                }
            }
        });

        addTextChangedListener(textWatcher);
    }

    public void disableTextWatcher() {
        removeTextChangedListener(textWatcher);
    }

    public void enableTextWatcher() {
        addTextChangedListener(textWatcher);
    }

    TextWatcher textWatcher = new TextWatcher() {
        boolean mToggle = false;

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(mToggle) {
                String text = SuggestionAutoCompleteView.this.getText().toString();
                getSuggestions(text);
                if(suggestionListener != null) {
                    suggestionListener.onChangedText(text);
                }
            }
            mToggle = !mToggle;
        }
    };

    abstract protected void getSuggestions(String text);

    Callback<List<T>> suggestionListListener = new Callback<List<T>>() {
        @Override
        public void onResponse(Call<List<T>> call, Response<List<T>> response) {
            List<T> suggestions = response.body();

            if(suggestions == null)
                return;

            updateSuggestionList(suggestions);
        }

        @Override
        public void onFailure(Call<List<T>> call, Throwable t) {

        }
    };

    private void updateSuggestionList(List<T> suggestions) {
        if(suggestions != null) {

            if(suggestions.isEmpty()) {
                suggestionAdapter.clear();
                dismissDropDown();

                return;
            }

            if((suggestions.size() == 1 && suggestions.get(0)
                    .equals(SuggestionAutoCompleteView.this.getText().toString())))
                return;

            if(clicked) {
                clicked = false;

                suggestionAdapter.clear();

                return;
            }

            if(mContext != null && mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }

            suggestionAdapter.clear();
            for(Suggestion sugg : suggestions)
                suggestionAdapter.add(sugg);
            suggestionAdapter.callFiltering();

            if(getRootView() != null) {
                SuggestionAutoCompleteView.this.showDropDown();
            }
        }
    }

    public void setClicked() {
        clicked = true;
    }

    public void setOnSuggestionChangedListener(OnSuggestionChangedListener listener) {
        this.suggestionListener = listener;
    }

    public interface OnSuggestionChangedListener {
        void onChangedText(String text);
        void onSelectedItem(Suggestion suggestion);
    }
}
