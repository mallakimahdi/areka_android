package ir.areka.app.custome_view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

import ir.areka.app.model.suggestion.AreaNameSuggestion;
import ir.areka.app.net.ArekaWebService;

/**
 * Created by mahdi on 7/6/16.
 */
public class AreaNameSuggestionCompleteView extends SuggestionAutoCompleteView {

    public AreaNameSuggestionCompleteView(Context context) {
        super(context);
        init(context);
    }

    public AreaNameSuggestionCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void getSuggestions(String text) {
        if(text != null && text.equals("")) {
            setSelectedArea(null);
        }
        else {
            ArekaWebService.getInstance().getWebService().getAreaNameSuggestions(text).enqueue(suggestionListListener);
        }
    }
/*
    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if(focused)
            getSuggestions("");
    }
*/
    public AreaNameSuggestion getSelectedArea() {
        return (AreaNameSuggestion) clickedItem;
    }

    public void setSelectedArea(AreaNameSuggestion suggestion) {
        clickedItem = suggestion;
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }


}
