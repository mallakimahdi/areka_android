package ir.areka.app.custome_view;

import android.content.Context;
import android.util.AttributeSet;

import ir.areka.app.net.ArekaWebService;

/**
 * Created by mahdi on 7/6/16.
 */
public class JobSuggestionCompleteView extends SuggestionAutoCompleteView {

    public JobSuggestionCompleteView(Context context) {
        super(context);
        init(context);
    }

    public JobSuggestionCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    protected void getSuggestions(String text) {
        ArekaWebService.getInstance().getWebService().getJobSuggestions(text).enqueue(suggestionListListener);
    }
}
