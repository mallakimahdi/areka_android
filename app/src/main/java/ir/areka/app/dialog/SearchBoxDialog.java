package ir.areka.app.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import java.util.List;

import ir.areka.app.R;
import ir.areka.app.custome_view.AreaNameSuggestionCompleteView;
import ir.areka.app.custome_view.JobSuggestionCompleteView;
import ir.areka.app.model.suggestion.AreaNameSuggestion;
import ir.areka.app.model.suggestion.Suggestion;

/**
 * Created by mahdi on 7/8/16.
 */
public class SearchBoxDialog extends DialogFragment {

    String query;
    Integer locationId;
    String locationName;
    JobSuggestionCompleteView edtJobName;
    AreaNameSuggestionCompleteView edtLocationName;

    public void setProperties(String query, Integer locationId, String locationName) {
        this.query = query;
        this.locationId = locationId;
        this.locationName = locationName;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View dialog = inflater.inflate(R.layout.dialog_search_box, container, false);
        edtJobName = (JobSuggestionCompleteView) dialog.findViewById(R.id.edt_search_input);
        edtLocationName = (AreaNameSuggestionCompleteView) dialog.findViewById(R.id.edt_search_location);

        if (query != null) {
            edtJobName.setText(query);
            edtJobName.setSelection(edtJobName.getText().length());
            edtJobName.setClicked();
        }
        if(locationName != null) {
            edtLocationName.setText(locationName);
            edtLocationName.setClicked();

            edtLocationName.setSelectedArea(new AreaNameSuggestion(locationId, locationName));
        }

        dialog.findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        return dialog;
    }

    public String getJobName() {
        return edtJobName.getText().toString();
    }

    public Suggestion getLocationSelected() {
        return edtLocationName.getSelectedArea();
    }
}
