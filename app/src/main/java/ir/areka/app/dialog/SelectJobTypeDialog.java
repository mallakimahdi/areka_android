package ir.areka.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.HashSet;
import java.util.Set;

import ir.areka.app.R;
import ir.areka.app.model.job.JobType;

/**
 * Created by mahdi on 5/20/16.
 */
public class SelectJobTypeDialog extends DialogFragment {

    private Set<JobType> selectedJobTypes;
    private JobTypeSelectListener jobTypeListener;
    private Context context;

    public void setSelectJobTypeDialog(Set<JobType> selectedJobTypes) {
        this.selectedJobTypes = new HashSet<>(selectedJobTypes);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View ll = LayoutInflater.from(context).inflate(R.layout.dialog_select_job_type, null);

        CheckBox chFullTime = ((CheckBox) ll.findViewById(R.id.checkbox_full_time));
        CheckBox chPartTime = ((CheckBox) ll.findViewById(R.id.checkbox_part_time));
        CheckBox chInternship = ((CheckBox) ll.findViewById(R.id.checkbox_internship));

        for(JobType jt : selectedJobTypes) {
            if(jt.name().equals(JobType.FULL_TIME.name()))
                chFullTime.setChecked(true);
            else if(jt.name().equals(JobType.PART_TIME.name()))
                chPartTime.setChecked(true);
            else if(jt.name().equals(JobType.INTERNSHIP.name()))
                chInternship.setChecked(true);
        }

        chFullTime.setOnCheckedChangeListener(checkListener);
        chPartTime.setOnCheckedChangeListener(checkListener);
        chInternship.setOnCheckedChangeListener(checkListener);

        ll.findViewById(R.id.ok_select_job_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jobTypeListener.onJobTypeSelected(selectedJobTypes);
                dialog.dismiss();
            }
        });

        ll.findViewById(R.id.cancel_select_job_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(ll);

        return dialog;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setJobTypeSelectListener(JobTypeSelectListener listener) {
        this.jobTypeListener = listener;
    }

    public interface JobTypeSelectListener {
        void onJobTypeSelected(Set<JobType> jobTypes);
    }


    CompoundButton.OnCheckedChangeListener checkListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton view, boolean b) {
            JobType type = null;
            if(view.getId() == R.id.checkbox_full_time)
                type = JobType.FULL_TIME;
            else if(view.getId() == R.id.checkbox_part_time)
                type = JobType.PART_TIME;
            else if(view.getId() == R.id.checkbox_internship)
                type = JobType.INTERNSHIP;

            if(view.isChecked())
                selectedJobTypes.add(type);
            else
                selectedJobTypes.remove(type);
        }
    };


}
