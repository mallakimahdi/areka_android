package ir.areka.app.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.model.comment.Comment;
import ir.areka.app.model.comment.CommentResult;
import ir.areka.app.model.company.Company;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCommentDialog extends Builder {
    private EditText edtName;
    private EditText edtComment;
    private AlertDialog dialog;
    private Company company;
    private List<View> imageViews;
    private int star = 1;
    private Comment commentToSend;
    Activity activity;

    public AddCommentDialog(Activity activity, Company company) {
        super(activity);
        this.activity = activity;
        this.company = company;
    }

    @Override
    public Builder setView(View view) {
        edtName = (EditText) view.findViewById(R.id.edt_comment_name);
        edtComment = (EditText) view.findViewById(R.id.edt_comment_comment);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        imageViews = new ArrayList<>(5);

        LinearLayout llStar = (LinearLayout) view.findViewById(R.id.ll_star);

        for (int i = 0; i < 5; i++) {
            View v = inflater.inflate(R.layout.view_star, null);
            ImageView img = (ImageView) v.findViewById(R.id.img_star_empty);
            img.setImageResource(R.drawable.ic_action_star_border);
            imageViews.add(img);
            imageViews.get(i).setOnClickListener(clickStar);
            imageViews.get(i).setTag(i);
            llStar.addView(v);
        }
        ((ImageView) imageViews.get(4).findViewById(R.id.img_star_empty)).setImageResource(R.drawable.ic_action_star);

        setPositiveButton(getContext().getResources().getString(R.string.add_comment), null);

        return super.setView(view);
    }

    OnClickListener clickBtnAddComment = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String nameText = edtName.getText().toString();
            String commentText = edtComment.getText().toString();
            if (nameText.equals(""))
                edtName.setBackgroundResource(R.drawable.edt_border);
            else
                edtName.setBackgroundResource(android.R.drawable.editbox_background_normal);
            if (commentText.equals(""))
                edtComment.setBackgroundResource(R.drawable.edt_border);
            else
                edtComment.setBackgroundResource(android.R.drawable.editbox_background_normal);

            if (!nameText.equals("") && !commentText.equals("")) {
                commentToSend = new Comment();
                commentToSend.setComment(edtComment.getText().toString() + "");
                commentToSend.setUsername(edtName.getText().toString() + "");
                commentToSend.setCompany(company);
                commentToSend.setStars(star);

                ArekaWebService.getInstance().getWebService().sendComment(commentToSend).enqueue(new Callback<CommentResult>() {
                    @Override
                    public void onResponse(Call<CommentResult> call, Response<CommentResult> response) {
                        Toast.makeText(getContext()
                                , getContext().getString(R.string.successfully_insert_comment)
                                , Toast.LENGTH_LONG).show();

                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<CommentResult> call, Throwable t) {
                        Toast.makeText(getContext(), getContext().getString(R.string.unknown_problem_occurred)
                                , Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    };

    @Override
    public AlertDialog create() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_add_comment, null);
        setView(view);

        dialog = super.create();
        dialog.show();

        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackgroundResource(R.drawable.btn_border_add);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(clickBtnAddComment);

        return dialog;
    }

    OnClickListener clickStar = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (Integer) v.getTag();
            star = 1;

            for (int i = 0; i <= 3; i++) {
                if (i < tag)
                    ((ImageView) imageViews.get(i).findViewById(R.id.img_star_empty)).setImageResource(R.drawable.ic_action_star_border);
                else {
                    ( (ImageView) imageViews.get(i).findViewById(R.id.img_star_empty)).setImageResource(R.drawable.ic_action_star);
                    star++;
                }
            }
        }
    };

}
