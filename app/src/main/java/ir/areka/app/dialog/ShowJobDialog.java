package ir.areka.app.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.activity.App;
import ir.areka.app.activity.BaseActivity;
import ir.areka.app.model.PageList;
import ir.areka.app.model.job.Job;
import ir.areka.app.net.ArekaWebService;
import ir.areka.app.net.WebServiceHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.CamcorderProfile.get;

/**
 * Created by mahdi on 5/18/17.
 */

public class ShowJobDialog extends DialogFragment {

    private BaseActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View dialog = inflater.inflate(R.layout.dialog_get_job, container, false);

        final EditText edtJobId = (EditText) dialog.findViewById(R.id.edt_job_id);
        Button btnGo = (Button) dialog.findViewById(R.id.btn_go);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = edtJobId.getText().toString();
                ArekaWebService.getInstance().getWebService().getJobsByIds(Arrays.asList(id))
                        .enqueue(new Callback<PageList<Job>>() {
                            @Override
                            public void onResponse(Call<PageList<Job>> call, Response<PageList<Job>> response) {
                                List<Job> jobs = response.body().getItems();
                                if(!jobs.isEmpty()) {
                                    activity.onJobSelected(null, jobs.get(0));
                                    dismiss();
                                }
                                else {
                                    Toast.makeText(App.getContext(), "not found", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<PageList<Job>> call, Throwable t) {

                            }
                        });
            }
        });

        return dialog;
    }

    public void setActivity(BaseActivity activity) {
        this.activity = activity;
    }
}
