package ir.areka.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by mahdi on 5/22/16.
 */
public class FilterSearchResultDialog extends DialogFragment {

    private Context context;
    private Integer distance;
    private SeekBar seekBarDistance;
    private TextView txtDisplayDistance;
    private OnDismissSelectDistanceDialog listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);

        //final View ll = LayoutInflater.from(context).inflate(R.layout.dialog_select_distance, null);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setContentView(ll);
/*
        txtDisplayDistance = (TextView) ll.findViewById(R.id.txt_display_distance);
        seekBarDistance = (SeekBar) ll.findViewById(R.id.seekbar_distance);

        final RadioButton rbSelectByDistance = (RadioButton) ll.findViewById(R.id.rb_select_by_distance);
        final RadioButton rbSelectByArea = (RadioButton) ll.findViewById(R.id.rb_select_by_area);

        final View llSearchByArea = ll.findViewById(R.id.ll_search_by_area);
        final View llSearchByDistance = ll.findViewById(R.id.ll_search_by_distance);

        rbSelectByArea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                llSearchByArea.setVisibility(b ? View.VISIBLE : View.GONE);
                if(b)
                    rbSelectByDistance.setChecked(false);
            }
        });
        rbSelectByDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                llSearchByDistance.setVisibility(b ? View.VISIBLE : View.GONE);
                if(b)
                    rbSelectByArea.setChecked(false);
            }
        });

        CheckBox distanceCheckBox = ((CheckBox) ll.findViewById(R.id.checkbox_any_distance));
        distanceCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                seekBarDistance.setVisibility(b ? View.GONE : View.VISIBLE);
                if(!b)
                    seekBarChangeListener.onProgressChanged(seekBarDistance, 0, false);
                else
                    txtDisplayDistance.setText("");
            }
        });

        seekBarDistance.setMax(100);
        if(distance == null)
            seekBarDistance.setProgress(45);
        else {
            seekBarDistance.setProgress(distance);
            distanceCheckBox.setChecked(false);
        }
        seekBarDistance.setOnSeekBarChangeListener(seekBarChangeListener);

        ll.findViewById(R.id.cancel_select_distance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ll.findViewById(R.id.ok_select_distance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer val = null;
                if(seekBarDistance.getVisibility() == View.VISIBLE)
                    val = seekBarDistance.getProgress();
                listener.onDismiss(val);
                dialog.dismiss();
            }
        });
*/
        return dialog;
    }

    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            txtDisplayDistance.setText(seekBar.getProgress()+" km");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    public FilterSearchResultDialog setContext(Context context) {
        this.context = context;
        return this;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public void setOnDismissSelectDistanceDialog(OnDismissSelectDistanceDialog listener) {
        this.listener = listener;
    }

    public interface OnDismissSelectDistanceDialog {
        void onDismiss(Integer distance);
    }
}
