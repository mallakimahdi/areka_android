package ir.areka.app.net;

import java.util.List;
import java.util.Map;

import ir.areka.app.model.UpdateDTO;
import ir.areka.app.model.comment.CommentResult;
import ir.areka.app.model.job.Job;
import ir.areka.app.model.job.NewJobsDTO;
import ir.areka.app.model.job.PopularJob;
import ir.areka.app.model.PageList;
import ir.areka.app.model.comment.Comment;
import ir.areka.app.model.comment.CommentDTO;
import ir.areka.app.model.company.Company;
import ir.areka.app.model.location.CityLocation;
import ir.areka.app.model.search.SearchHistory;
import ir.areka.app.model.suggestion.AreaNameSuggestion;
import ir.areka.app.model.suggestion.JobSuggestion;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by mahdi on 11/18/16.
 */

public interface JobServiceInterface {

    @GET("job.php")
    Call<List<JobSuggestion>> getJobSuggestions(@Query("jobSuggest") String text);

    @GET("job.php")
    Call<List<AreaNameSuggestion>> getAreaNameSuggestions(@Query("areaNameSuggest") String text);

    @GET("job.php")
    Call<PageList<Job>> searchJobs(@QueryMap Map<String, String> options);

    @POST("job.php?jobIds")
    Call<PageList<Job>> getJobsByIds(@Body List<String> jobIds);

    @GET("job.php?countAllNewJobsLastWeek")
    Call<NewJobsDTO> getNewJobsLastWeek();

    @GET("job.php?sortType=date")
    Call<PageList<Job>> getCompanyJobs(@Query("companyId") String companyId);

    @POST("job.php?search_history")
    Call<List<Integer>> getSearchHistory(@Body List<SearchHistory> searchHistories);

    @POST("job.php?favoriteJobs")
    Call<PageList<Job>> getFavoriteJobs(@Body List<String> favoriteJobs);

    @GET("job.php?whereAmI")
    Call<CityLocation> getLocationNameByLocationPoint(@Query("lat") double lat, @Query("lon") double lon);

    @GET("job.php")
    Call<PageList<Job>> getSimilarResult(@Query("similarityDoc") String similarityDoc);

    @GET("job.php?popularJobs")
    Call<List<PopularJob>> getPopularJobs();

    @POST("company.php?newComment")
    Call<CommentResult> sendComment(@Body Comment comment);

    @GET("company.php?getComments")
    Call<List<CommentDTO>> getComments(@Query("companyId") String id, @Query("page") int requestPart);

    @GET("company.php")
    Call<Company> getCompanyById(@Query("companyId") String companyId);

    @GET("statistics.php")
    Call<ResponseBody> interactWithJob(@QueryMap Map<String, String> options);

    //@GET("checkUpdate.php")
    //Call<UpdateDTO> checkUpdate(@Query("deviceUuid") String deviceUuid, @Query("versionName") String versionName);

    @GET("statistics.php")
    Call<ResponseBody> sendFirebaseToken(@Query("firebaseToken") String firebaseToken);
}
