package ir.areka.app.net;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mahdi on 3/22/16.
 */
public class WebServiceHandler {

    private static WebServiceHandler webServiceHandler;
    private static Handler mHandler;

    ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public static synchronized WebServiceHandler getInstance() {
        if (webServiceHandler == null) {
            webServiceHandler = new WebServiceHandler();
            mHandler = new Handler(Looper.getMainLooper());
        }
        return webServiceHandler;
    }

    public void getLocationInfo(Location location, DownloadListener listener) {
        concurrentDownloadJson("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.getLatitude()
                        + "," + location.getLongitude() + "&sensor=true"
                , null, listener);
    }

    public void getLocationCity(DownloadListener listener) {
        concurrentDownloadJson("https://freegeoip.net/json", null, listener);
    }

    public interface DownloadListener {
        void onComplete(String str);
        void onFailed(int status);
    }

    private synchronized void concurrentDownloadJson(final String address, final String requestBody
            , final DownloadListener listener) {
        executor.submit(new Runnable() {
                            @Override
                            public void run() {
                                downloadJson(address, requestBody, listener);
                            }
                        }
        );
    }

    private void downloadJson(final String address, String requestBody, final DownloadListener listener) {

        try {
            URL url = new URL(address);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(15000);

            if (requestBody != null) {
                try {
                    connection.setRequestMethod("POST");
                    OutputStream outputStream = connection.getOutputStream();
                    outputStream.write(requestBody.getBytes("UTF-8"));
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                connection.setRequestMethod("GET");
            }

            int statusCode = connection.getResponseCode();

            if (statusCode != 200)
                throw new Exception(statusCode + "");

            InputStream in = new BufferedInputStream(connection.getInputStream());
            final String responseStr = buildResponseFromEntity(in);

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(listener != null)
                        listener.onComplete(responseStr);
                }
            });
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(listener != null)
                        listener.onFailed(503);
                }
            });
        } catch (final Exception e) {
            e.printStackTrace();
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(listener == null)
                        return;

                    if (e.getMessage() != null && e.getMessage().equals(404 + ""))
                        listener.onFailed(404);
                    else
                        listener.onFailed(500);
                }
            });
        }
    }

    private String buildResponseFromEntity(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String read = br.readLine();

        while (read != null) {
            sb.append(read);
            read = br.readLine();
        }
        return sb.toString();
    }
}
