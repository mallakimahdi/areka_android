package ir.areka.app.net;

import java.io.IOException;

import ir.areka.app.activity.App;
import ir.areka.app.model.location.UserCity;
import ir.areka.app.util.LocationUtil;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mahdi on 11/18/16.
 */

public class ArekaWebService {

    //public static final String BASE_API_URL = "http://89.184.195.186/areka/api/";
    public static final String BASE_API_URL = "http://areka.ir/api/";

    private final JobServiceInterface serviceInterface;
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new HttpLoggingInterceptor()
                            .setLevel(HttpLoggingInterceptor.Level.BODY))
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            HttpUrl originalHttpUrl = original.url();

                            HttpUrl.Builder urlBuilder = originalHttpUrl.newBuilder();
                            urlBuilder.addQueryParameter("deviceUuid", App.getDeviceUuid());
                            urlBuilder.addQueryParameter("versionName", App.getVersionName());

                            UserCity latestLocation = LocationUtil.getLatestUserLocation();
                            if(latestLocation != null) {
                                urlBuilder.addQueryParameter("location", latestLocation.getLat() +","+ latestLocation.getLon());
                            }

                            Request.Builder requestBuilder = original.newBuilder().url(urlBuilder.build());
                            Request request = requestBuilder.build();

                            return chain.proceed(request);
                        }
                    })
                    .build())
            .build();

    private static ArekaWebService service;

    public static ArekaWebService getInstance() {
        if (service == null)
            service = new ArekaWebService();
        return service;
    }

    public ArekaWebService() {
        serviceInterface = retrofit.create(JobServiceInterface.class);
    }

    public JobServiceInterface getWebService() {
        return serviceInterface;
    }

}
