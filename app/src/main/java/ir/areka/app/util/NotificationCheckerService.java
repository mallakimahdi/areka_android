package ir.areka.app.util;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by mahdi on 11/2/16.
 */

public class NotificationCheckerService extends Service {

    Context mContext;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkUpdate();
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mContext = getApplicationContext();
        mContext.registerReceiver(receiver, new IntentFilter(Intent.ACTION_SCREEN_ON));

        try {
            checkUpdate();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }

    public synchronized void checkUpdate() {
        NotificationReceiverThread thread = NotificationReceiverThread.getInstance();
        thread.setContext(mContext);

        if(!thread.isRunning()) {
            try {
                thread.start();
            }
            catch (Exception e) {
                thread.update();
            }
        }
        else {
            thread.update();
        }
    }
}
