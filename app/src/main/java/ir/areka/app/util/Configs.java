package ir.areka.app.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mahdi on 11/2/16.
 */

public class Configs {

    private static Configs instance;
    private static Context mContext;
    private final SharedPreferences prefs;

    public static Configs getInstance(Context context) {
        if(instance == null)
            instance = new Configs(context);
        mContext = context;
        return instance;
    }

    public Configs(Context context) {
        this.mContext = context;
        prefs = mContext.getSharedPreferences("areka", Context.MODE_PRIVATE);
    }

    public long getLastCheckNewJobsDate() {
        return prefs.getLong("checkNewJobs", 0l);
    }

    public void setLastCheckNewJobsDate(long date) {
        prefs.edit().putLong("checkNewJobs", date).apply();
    }

    public long getCheckUpdatePeriodDate() {
        //default value is 1 day
        return prefs.getLong("checkUpdatePeriod", 24 * 60 * 60 * 1000);
    }

    public void setCheckUpdatePeriodDate(long date) {
        prefs.edit().putLong("checkUpdatePeriod", date).apply();
    }

    public void setShowTooltipSearch(boolean isShow) {
        prefs.edit().putBoolean("show_tooltip_search", isShow).apply();
    }

    public void setShowTooltipSearchResult(boolean isShow) {
        prefs.edit().putBoolean("show_tooltip_search_result", isShow).apply();
    }

    public boolean isFirebaseTokenSent() {
        return prefs.getBoolean("firebaseTokenSent", false);
    }

    public void setFirebaseTokenSent() {
        prefs.edit().putBoolean("firebaseTokenSent", true).apply();
    }
}
