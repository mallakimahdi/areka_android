package ir.areka.app.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Toast;

import java.util.List;

import ir.areka.app.R;
import ir.areka.app.model.job.Job;

/**
 * Created by mahdi on 8/16/16.
 */
public class ShareUtil {

    private static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void shareToTwitter(Context context, Job job) {
        Intent shareIntent;

        String twitterShareText = job.getTitle() + "\n" + "http://areka.ir/" + job.getId() + "\n" + "@areka_job";
        shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/*");
        shareIntent.putExtra(Intent.EXTRA_TEXT, twitterShareText);

        if (isAppAvailable(context, "com.twitter.android")) {

            PackageManager packManager = context.getPackageManager();
            List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);

            boolean resolved = false;
            for (ResolveInfo resolveInfo : resolvedInfoList) {
                if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                    shareIntent.setClassName(
                            resolveInfo.activityInfo.packageName,
                            resolveInfo.activityInfo.name);
                    resolved = true;
                    break;
                }
            }
            if (resolved) {
                context.startActivity(shareIntent);
            } else {
                String tweetUrl = "https://twitter.com/intent/tweet?text=" + twitterShareText;
                Uri uri = Uri.parse(tweetUrl);
                shareIntent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(shareIntent);
            }
        } else {
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + twitterShareText;
            Uri uri = Uri.parse(tweetUrl);
            shareIntent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(shareIntent);
        }

    }

    public static void shareToTelegram(Context context, Job job) {
        String telegramName = "org.telegram.messenger";

        if (isAppAvailable(context, telegramName)) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage(telegramName);
            intent.putExtra(Intent.EXTRA_TEXT, shareText(context, job));

            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Telegram not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    public static String shareText(Context context, Job job) {
        StringBuilder builder = new StringBuilder();

        builder.append(job.getTitle()).append("\n\n");

        for (String s : job.getBody()) {
            builder.append(s).append("\n");
        }

        if (job.getAddress() != null && !job.getAddress().isEmpty()) {
            builder.append(context.getString(R.string.address)).append(": ")
                    .append(job.getAddress()).append("\n");
        }

        if (job.getEmail() != null && !job.getEmail().isEmpty()) {
            builder.append(context.getString(R.string.email)).append("\n");
            for (String s : job.getEmail())
                builder.append(s).append("\n");
        }

        if (job.getPhoneNumber() != null && !job.getPhoneNumber().isEmpty()) {
            builder.append(context.getString(R.string.tell)).append(": ").append("\n");
            for (String s : job.getPhoneNumber())
                builder.append(s).append("\n");
        }

        builder
            .append("\n\n\n")
                .append(context.getString(R.string.share_desc))
                .append("\n")
                .append(context.getString(R.string.download_app) + ": " + context.getString(R.string.download_app_link))
        ;

        return builder.toString();
    }
}
