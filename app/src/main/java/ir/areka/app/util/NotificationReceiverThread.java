package ir.areka.app.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.List;

import ir.areka.app.R;
import ir.areka.app.activity.App;
import ir.areka.app.activity.MainActivity;
import ir.areka.app.activity.SearchResultActivity;
import ir.areka.app.model.search.SearchHistory;
import ir.areka.app.model.search.SearchMgr;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Response;

/**
 * Created by mahdi on 11/2/16.
 */

public class NotificationReceiverThread extends Thread {

    private Context mContext;

    private boolean isRunning;

    public static NotificationReceiverThread instance;

    public static NotificationReceiverThread getInstance() {
        if (instance == null)
            instance = new NotificationReceiverThread();
        return instance;
    }

    @Override
    public void run() {
        super.run();

        isRunning = true;

        while (true) {

            long lastCheckUpdate = Configs.getInstance(mContext).getLastCheckNewJobsDate();
            long checkUpdatePeriod = Configs.getInstance(mContext).getCheckUpdatePeriodDate();

            if (checkUpdatePeriod != -1 && lastCheckUpdate < System.currentTimeMillis() - checkUpdatePeriod) {
                checkUpdate();
            }

            synchronized (this) {
                try {
                    long period = Configs.getInstance(mContext).getCheckUpdatePeriodDate();
                    if(period == -1) {
                        break;
                    }
                    wait(period);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    private void checkUpdate() {
        final List<SearchHistory> searchHistories = SearchMgr.getInstance().getSearchHistories();

        if(searchHistories.isEmpty()) {
            return;
        }

        Response<List<Integer>> response = null;
        try {
            response = ArekaWebService.getInstance().getWebService().getSearchHistory(searchHistories).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response == null || response.body() == null || response.body().isEmpty())
            return;

        SearchHistory selectedSearchHistory = null;
        for (int i=0; i < response.body().size(); i++) {

            Integer newCount = response.body().get(i);

            if(newCount > 0) {
                selectedSearchHistory = searchHistories.get(i);
                selectedSearchHistory.setNewJobs(newCount);
                break;
            }
        }

        if(selectedSearchHistory == null) {
            return;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setContentTitle(mContext.getString(R.string.new_jobs_notif))
                .setContentText(selectedSearchHistory.getNewJobs() + " "
                        + mContext.getString(R.string.new_jobs_desc_notif) +" "
                        + selectedSearchHistory.getQuery() + " "
                        + mContext.getString(R.string.received) + "\n"
                        + mContext.getString(R.string.disable_notification))
                .setSmallIcon(R.mipmap.logo)
                //.setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true);

        Intent resultIntent = new Intent(App.getContext(), SearchResultActivity.class);
        resultIntent.putExtra("query", selectedSearchHistory.getQuery());
        resultIntent.putExtra("locationId", StringUtils.join(selectedSearchHistory.getLocationIds()
                , ","));
        resultIntent.putExtra("locationName", selectedSearchHistory.getLocationName());
        resultIntent.putExtra("lastSearchDate", selectedSearchHistory.getLastUpdateDate());
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(App.getContext());
        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0
                , PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) App.getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = mBuilder.build();
        mNotificationManager.notify(1, notification);

        Configs.getInstance(mContext).setLastCheckNewJobsDate(System.currentTimeMillis());
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void update() {
        synchronized (this) {
            notify();
        }
    }
}
