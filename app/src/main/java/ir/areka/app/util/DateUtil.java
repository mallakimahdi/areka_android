package ir.areka.app.util;

import java.util.concurrent.TimeUnit;

import ir.areka.app.R;
import ir.areka.app.activity.App;

/**
 * Created by mahdi on 5/28/16.
 */
public class DateUtil {

    static WordNormalizer normalizer = WordNormalizer.getInstance();

    public static String getTimeAgo(long date) {
        long deferenceTime = System.currentTimeMillis() - date;

        if(deferenceTime <= (24 * 60 * 60 * 1000)) {

            if(deferenceTime < (60 * 60 * 1000)) {

                if(deferenceTime < 60 * 1000) {
                    return App.getContext().getString(R.string.moment_ago);
                }
                else {
                    long minuteAgo = TimeUnit.MILLISECONDS.toMinutes(deferenceTime);
                    return normalizer.toArabicDigits(minuteAgo) + " " + App.getContext().getString(R.string.minute_ago);
                }

            }
            else {
                long hoursAgo = TimeUnit.MILLISECONDS.toHours(deferenceTime);
                return normalizer.toArabicDigits(hoursAgo) + " " + App.getContext().getString(R.string.hour_ago);
            }
        }
        else {

            long daysAgo = TimeUnit.MILLISECONDS.toDays(deferenceTime);

            if (daysAgo < 7) {

                if (daysAgo == 1)
                    return App.getContext().getString(R.string.yesterday);
                else
                    return normalizer.toArabicDigits(daysAgo) + " " + App.getContext().getString(R.string.day_ago);
            }
            else
                return normalizer.toArabicDigits(daysAgo / 7) + " " + App.getContext().getString(R.string.week_ago);
        }
    }

}
