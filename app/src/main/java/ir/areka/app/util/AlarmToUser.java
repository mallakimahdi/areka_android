package ir.areka.app.util;

import android.content.Context;
import android.widget.Toast;

import ir.areka.app.R;

/**
 * Created by mahdi on 11/14/16.
 */

public class AlarmToUser {

    public static void problemSendRequest(Context context, int status) {

        int message;
        if(status == 503) {
            message = R.string.please_connect_your_device_to_internet;
        }
        else {
            message = R.string.occurred_problem_try_again;

        }

        Toast.makeText(context, context.getString(message), Toast.LENGTH_SHORT).show();
    }

}
