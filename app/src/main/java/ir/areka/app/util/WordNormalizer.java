package ir.areka.app.util;

import java.text.DecimalFormat;

import ir.areka.app.R;
import ir.areka.app.activity.App;

public class WordNormalizer
{
	private static WordNormalizer wordNormalizer;
	private Character[] chars = {App.getContext().getResources().getString(R.string.sefr).charAt(0)
			,App.getContext().getResources().getString(R.string.yek).charAt(0)
			,App.getContext().getResources().getString(R.string.dow).charAt(0)
			,App.getContext().getResources().getString(R.string.se).charAt(0)
			,App.getContext().getResources().getString(R.string.chahar).charAt(0)
			,App.getContext().getResources().getString(R.string.panj).charAt(0)
			,App.getContext().getResources().getString(R.string.shesh).charAt(0)
			,App.getContext().getResources().getString(R.string.haft).charAt(0)
			,App.getContext().getResources().getString(R.string.hasht).charAt(0)
			,App.getContext().getResources().getString(R.string.noh).charAt(0)};

	public static WordNormalizer getInstance()
	{
		if(wordNormalizer == null) {
			wordNormalizer = new WordNormalizer();
		}
		return wordNormalizer;
	}
	
	public String toArabicDigits(long amount)
	{
        try {
            DecimalFormat formatter = new DecimalFormat("#,###");
            String ss = formatter.format(amount);

            StringBuffer builder = new StringBuffer();

            for(int i=0; i<ss.length(); i++)
            {
                if(Character.isDigit(ss.charAt(i))) {
                    int m = Character.getNumericValue(ss.charAt(i));
                    builder.append(chars[m]);
                }
                else
                    builder.append(ss.charAt(i));
            }

            builder.trimToSize();
            return builder.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return amount+"";
	}
}
