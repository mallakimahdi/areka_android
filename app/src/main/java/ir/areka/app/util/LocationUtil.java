package ir.areka.app.util;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;

import ir.areka.app.activity.App;
import ir.areka.app.model.location.CityLocation;
import ir.areka.app.model.location.UserCity;
import ir.areka.app.net.ArekaWebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahdi on 4/3/17.
 */

public class LocationUtil {

    static LocationManager locationManager = (LocationManager) App.getContext()
            .getSystemService(App.getContext().LOCATION_SERVICE);

    static long lastRequestedLocation = 0l;

    static SharedPreferences preferences = App.getContext().getSharedPreferences("areka", Context.MODE_PRIVATE);

    static Location latestReceivedLocation = null;

    public static boolean isNetworkProviderExists() {
        return locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER);
    }

    public static void getLocation(final LocationReceivedListener locationListener) {

        if (ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(App.getContext()
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        String provider = locationManager.getBestProvider(criteria, true);

        Location bestLocation;
        if (provider != null)
            bestLocation = locationManager.getLastKnownLocation(provider);
        else
            bestLocation = null;

        Location latestLocation = getLatest(bestLocation,
                locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

        latestLocation = getLatest(latestLocation,
                locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

        latestLocation = getLatest(latestLocation,
                locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER));

        lastRequestedLocation = System.currentTimeMillis();

        if (latestLocation != null) {
            getCity(locationListener, latestLocation);
        }
        else if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {

            final LocationListener locListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    latestReceivedLocation = location;
                    getCity(locationListener, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                }

                @Override
                public void onProviderDisabled(String s) {
                }
            };

            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locListener, null);
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locListener, null);

            Looper looper = Looper.myLooper();
            new Handler(looper).postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(latestReceivedLocation == null) {
                        locationListener.onTimeOutGetLocation();
                    }
                }
            }, 10_000);

            new Handler(looper).postDelayed(new Runnable() {
                @Override
                public void run() {
                    locationManager.removeUpdates(locListener);
                }
            }, 120_000);
        }
        else {
            locationListener.locationReceived(null, null);
        }
    }

    private static void getCity(final LocationReceivedListener locationListener, final Location location) {
        ArekaWebService.getInstance().getWebService().getLocationNameByLocationPoint(location.getLatitude()
                , location.getLongitude()).enqueue(new Callback<CityLocation>() {
            @Override
            public void onResponse(Call<CityLocation> call, Response<CityLocation> response) {
                CityLocation loc = response.body();

                if(loc != null) {
                    saveUserLocation(new UserCity(loc.getId(), loc.getCity(), System.currentTimeMillis()
                            , location.getLatitude(), location.getLongitude()));
                }

                if(lastRequestedLocation + 10_000 > System.currentTimeMillis()) {
                    if (loc != null) {
                        locationListener.locationReceived(loc.getId(), loc.getCity());
                    } else {
                        locationListener.locationReceived(null, null);
                    }
                }
            }

            @Override
            public void onFailure(Call<CityLocation> call, Throwable t) {
            }
        });
    }

    private static void saveUserLocation(UserCity location) {
        preferences.edit().putString("user_location", location.getId() +","
                + location.getCity() +","+ location.getDate()).apply();
    }

    public static UserCity getLatestUserLocation() {
        String userLoc = preferences.getString("user_location", null);
        if(userLoc != null) {
            String[] segments = userLoc.split(",");
            return new UserCity(Integer.valueOf(segments[0]), segments[1], Long.valueOf(segments[2]));
        }

        return null;
    }

    private static Location getLatest(final Location location1, final Location location2) {
        if (location1 == null)
            return location2;

        if (location2 == null)
            return location1;

        if (location2.getTime() > location1.getTime())
            return location2;
        else
            return location1;
    }

    public interface LocationReceivedListener {
        void locationReceived(Integer locId, String locName);
        void onTimeOutGetLocation();
    }
}
