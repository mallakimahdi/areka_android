package ir.areka.app.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.support.v4.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import ir.areka.app.R;
import ir.areka.app.activity.App;

/**
 * Created by mahdi on 4/23/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("fcm service", "from: "+ remoteMessage.getFrom() + ", message: "+ remoteMessage
                .getNotification().getBody());

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(App.getContext());
        mBuilder.setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setSmallIcon(R.mipmap.logo)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true);

        NotificationManager mNotificationManager = (NotificationManager) App.getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        if(remoteMessage.getData().containsKey("url")) {
            Intent notificationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(remoteMessage
                    .getData().get("url")));
            PendingIntent contentIntent = PendingIntent.getActivity(App.getContext(), 0, notificationIntent, 0);
            mBuilder.setContentIntent(contentIntent);
        }

        Notification notification = mBuilder.build();

        mNotificationManager.notify(1, notification);
    }
}
